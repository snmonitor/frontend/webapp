# Sn-monitor Frontend

Ce repo contient le code source du frontend de Sn-monitor.

## Exigence

- [node.js](https://nodejs.org/en/)

## Structure

- **Dossier** `src/`: contient les differents dossier pour la logique (state / traitement) et le designe (components /
  page)

---

- **Dossier** `src/redux/`: responsable du management des states de l'application

---

- **Dossier** `src/components/`: contient les differents component de l'application

---

- **Fichier** `index.ts`: c'est le point d'entrer de l'application (main).

---

- **Fichier** `app.ts`: il est responsable de la navigation de l'application

---

## Demarrage

- pour demarrer le frontend il faut tout d'abord ajouter un fichier env.js dans le path suivant `src/env.js`
  apres il faut ajouter les variables d'environnement en se basant sur le fichier `env_example.js`

#### variable d'environnement

- l'url vers l'api <br/>
  `export const BASE_URL = "http://localhost/api"`
- secret pour hasher les données dans local storage<br/>
  `export const SECRET = "mySecret"`

apres avoir configurer le `env.js` il reste qu'a entrer les commandes suivantes

```shell script
$> yarn install
```

```shell script
$> yarn start
```

