import './App.css';
import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { AnalytiquePage, DashboardPage, DriverPage, Error404Page, LoginPage, ScanPage } from "./pages";
import Navigation from "./Navigation";
import { pages } from "./enums";
import { GuardedRoute } from "./components";


function App() {

    const error = useSelector(state => state.error);
    const auth = useSelector(state => state.auth);
    return <>
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Navigation error={error} auth={auth} />}>
                    <Route exact path={`/${pages.Dashboard}`}
                        element={<GuardedRoute logged={auth.logged}
                            toPage={pages.Login}><DashboardPage /></GuardedRoute>} />
                    <Route exact path={`/${pages.Analytique}`}
                        element={<GuardedRoute logged={auth.logged}
                            toPage={pages.Login}><AnalytiquePage /></GuardedRoute>} />
                    <Route exact path={`/${pages.Scan}`}
                        element={<GuardedRoute logged={auth.logged}
                            toPage={pages.Login}><ScanPage /></GuardedRoute>} />
                    <Route exact path={`/${pages.Driver}`}
                        element={<GuardedRoute logged={auth.logged}
                            toPage={pages.Login}><DriverPage /></GuardedRoute>} />

                    <Route exact path={`/${pages.Driver}`}
                        element={<GuardedRoute logged={auth.logged}
                            toPage={pages.Login}><Error404Page /></GuardedRoute>} />
                    <Route exact path={`/${pages.Login}`}
                        element={<GuardedRoute logged={!auth.logged} toPage={pages.Dashboard}><LoginPage
                            auth={auth} /></GuardedRoute>} />

                    <Route path='*' element={<Error404Page />} />
                </Route>


            </Routes>
        </BrowserRouter>
    </>
}

export default App
