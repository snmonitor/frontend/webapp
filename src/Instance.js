import axios from "axios";
import { throwAxiosError } from "./errors/errorManager";
import { SOCKET_URL, BASE_URL } from "./env";
import User from "./entities/user";
import io from "socket.io-client";
import { kpiReceivedAction } from "./redux/kpi/kpi_action";
import { userCountAction } from "./redux/user/user_action";
export default class Instance {
    static socket
    static launcheSocket(dispatch) {

        this.socket = io(SOCKET_URL);
        this.socket?.on("update", (data) => {
            dispatch(kpiReceivedAction(data))
        });
        this.socket?.on("users", (data) => {
            dispatch(userCountAction(data))
        });

        // this.socket?.on("drivers", (data) => {
        //     dispatch(getAllDriverAction(data))
        // });

    }




    static getAxios() {
        if (this.axiosInstance)
            return this.axiosInstance
        else {
            this.axiosInstance = axios.create();
            this.axiosInstance.interceptors.request
                .use(function (config) {
                    config.headers = {
                        Accept: '*/*',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${User.accessToken}`
                    }
                    config.validateStatus = () => {
                        return true;
                    };
                    config.baseURL = `${BASE_URL}`
                    config.timeout = 15000
                    return config
                }, function (error) {
                    throwAxiosError(error)
                });
            return this.axiosInstance
        }
    }

}