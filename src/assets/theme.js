const colors0 = "#D1D1D1"
const colors1 = "#ff6b6b"
const colors2 = "#d7dae2"
const colors3 = "#fef5dd"
const colors4 = "#657997"
const colors5 = "#F0E9D2"
const colors6 = "#3a4a74"
const colors7 = "#EBEBE3"
const colors8 = "#F4F4F4"
const colors9 = "#2b2b2b"
const colors10 = "#826F66"
const colors11 = "#19B0F4"
const colors12 = "#FFA117"
const colors13 = "#5CB65F"
const colors14 = "#CF6679"
const colors15 = "#0B0B0B"
const colors16 = "#1E3FD1"
const colors17 = "#222831"
const colors18 = "#5893D4"
const colors19 = "#1E1E1E"
const colors20 = "#d1d7eb"
const colors21 = "#9e9b9b"
const colors22 = "#FFFFFF"


const th = {
    LIGHT: {
        fontFamily: "Karla,Serif",
        colorDisabled: colors0,
        colorError: colors1,
        colorShadow: colors2,
        colorHeader: colors3,
        colorHeaderText: colors4,
        colorButton: colors5,
        colorButton2: colors6,
        colorRow: colors22,
        colorRowHover: colors6,
        colorRowHoverText: colors3,
        colorCrudHover: colors10,
        colorChart: colors6,
        colorBg: colors8,
        color0: colors9,
        color1: colors5,
        color2: colors3,
        color3: colors10,
        color4: colors6,
        color5: colors6,
        color6: colors21,
        colorInfo: colors11,
        colorWarning: colors12,
        colorSuccess: colors13
    },
    DARK: {
        fontFamily: "Karla,Serif",
        colorDisabled: colors0,
        colorError: colors14,
        colorShadow: colors15,
        colorHeader: colors16,
        colorHeaderText: colors7,
        colorButton: colors7,
        colorButton2: colors16,
        colorRow: colors17,
        colorRowHover: colors16,
        colorRowHoverText: colors7,
        colorCrudHover: colors20,
        colorChart: colors18,
        colorBg: colors19,
        color0: colors9,
        color1: colors17,
        color2: colors17,
        color3: colors21,
        color4: colors7,
        color5: colors20,
        color6: colors21,
        colorInfo: colors11,
        colorWarning: colors12,
        colorSuccess: colors13
    }
}

export function getTheme(theme, color) {
    const t = theme ?? "DARK"
    return th[t][color] ?? th.DARK[color]
}