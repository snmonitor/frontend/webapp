import styled from 'styled-components'

export const AlertStyle=styled.div`
display: flex;
min-width: 30vw;
border-radius: 10px;
justify-content: center;
align-items: center;
padding: 7px ;
margin: 3px;
position:fixed;
bottom: 0;
right: 0;
height: 10vh;
background: ${p=>p.severity.color};
transition-duration: 0.7s;
transform:scale(${p=>p.size});
z-index: 10;
`