import styled from 'styled-components'


export const Center = styled.div`
  height: ${props => props.height ?? '100%'};
  width: ${props => props.width ?? '100%'};
  justify-content: center;
  display: flex;
  align-items: center;
`