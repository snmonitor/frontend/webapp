import { FormControl, Input, InputLabel, MenuItem, Select } from "@mui/material";
import { getTheme } from "../../../assets/theme";
import { useEffect } from "react";
import { v4 as uuid } from 'uuid'
import { useSelector } from "react-redux";

export default function InputSelect(props) {
    const thememode = useSelector(state => state.theme)
    useEffect(() => {
        if (props.setValue)
            props.setValue(props.default ?? props.values[0]?.value)
    }, [])

    return <div
        style={{
            width: `${props.width ?? "100%"}`,
            padding: `${props.padding ?? "0px"}`,
        }}><FormControl sx={{ width: "100%" }}>
            <InputLabel style={{
                padding: "10px 0 0 0",
                color: `${getTheme(thememode, "color6")}`
            }} variant="outlined">
                {props.label}
            </InputLabel>
            <Select
                value={props.default ?? props.values[0]?.value}
                onChange={(v) => {
                    props.setValue(v.target?.value)
                }}
                style={{
                    padding: "0 0 0 13px",
                    ...props.style
                }}
                input={<Input />}
            >
                {props.values?.map((data) => (<MenuItem
                    key={uuid()}
                    value={data?.value}
                >
                    {data?.name}
                </MenuItem>))}
            </Select>
        </FormControl></div>
}