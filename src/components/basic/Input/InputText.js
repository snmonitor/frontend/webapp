import { FilledInput, FormControl, Icon, InputAdornment, InputLabel } from "@mui/material";
import { getTheme } from "../../../assets/theme";
import * as React from "react";
import { useState, useMemo, } from "react";
import { useSelector } from "react-redux";

export default function InputText(props) {

    const thememode = useSelector(state => state.theme)
    const [error, setError] = useState(false)

    function handleError(v) {
        setError(v.length <= 0)
    }
    function render() {
        return <div
            style={{
                width: "100%",
                padding: `${props.padding ?? "20px"}`,
                ...props.style
            }}><FormControl
                sx={{ width: "100%" }}
                variant="filled"
            >
                <InputLabel
                    sx={{

                        color: props.labelColor ??
                            getTheme(thememode, "color3"),
                    }}
                    error={error}
                >{props.label}
                </InputLabel>
                <FilledInput

                    onChange={(e) => {
                        handleError(e.target.value)
                    }}
                    onBlur={(e) => {
                        props.setValue(e.target.value)
                    }}
                    defaultValue={props.value}
                    type={props.type}
                    placeholder={props.placeholder}
                    inputProps={{
                        maxLength: props.maxLength,
                        style: {
                            backgroundColor: props.backColor ??
                                getTheme(thememode, "color1"),
                            color: props.color ??
                                getTheme(thememode, "color0"),
                            height: props.height,
                            padding: props.padding,
                            maring: props.maring
                        },
                    }}
                    required={props.required ?? true}
                    fullWidth
                    name={props.name}
                    id={props.id}
                    error={error}
                    endAdornment={
                        !props.hideIcon && <InputAdornment position="end">
                            <Icon
                                edge="end"
                            >
                                {props.icon}
                            </Icon>
                        </InputAdornment>
                    }

                />
            </FormControl></div>
    }
    return useMemo(() => render(), [props])
}