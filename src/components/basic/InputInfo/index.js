import { CircularProgress } from "@mui/material";
import { useSelector } from "react-redux";
import { getTheme } from "../../../assets/theme";
import { ButtonStyle, Column, MiniText } from "../../style";

export default function InputInfo(props) {
     const thememode = useSelector(state => state.theme)
     return (<Column
          style={{ padding: "7px" }}
          hori="center"
     ><MiniText
          thememode={thememode}
          style={{
               whiteSpace: 'nowrap',
               overflow: 'hidden',
               color: getTheme(thememode, "color4")
          }}>{props.label}</MiniText>
          {props.data ? <ButtonStyle
               thememode={thememode}
               shadow
               primary={getTheme(thememode, "color4")}
               secondary={getTheme(thememode, "color2")}
               style={{ border: "dashed 0px", width: "100%", padding: "0" }}
          >{props.data}</ButtonStyle> : <CircularProgress style={{
               color: `${getTheme(thememode, "color4")}`, width: "20px", height: "20px"
          }} />}
     </Column>)
}