import { MiniButtonStyle } from "./style";
import { Column } from "../../style";
import { Delete, Setting } from "../../../assets/icons";
import { getTheme } from '../../../assets/theme'
import { useSelector } from "react-redux";
import { ScanCard } from "../..";
import { useMemo } from "react";

export default function ListCrud(props) {
    const driverTemplates = useSelector(state => state.driverTemplate?.driverT)

    const thememode = useSelector(state => state.theme)
    const iconsW = "23px"
    const iconsH = "23px"
    const render = () => {
        return <Column hori={"flex-end"} style={{ width: "3%", alignSelf: "flex-start", }}>
            <div
                style={{ width: '100%', margin: 0, padding: 0 }}>
                <MiniButtonStyle thememode={thememode}
                    h={iconsH} w={iconsW} onClick={() => props.deleteFn(props._id)}>
                    <Delete style={{ height: "100%", margin: 0, padding: 0 }} /></MiniButtonStyle></div>
            <ScanCard
                style={{ width: '100%', margin: 0, padding: 0 }}
                submitFn={props.settingFn}
                data={props.data}
                driverTemplates={driverTemplates}
                comp={<MiniButtonStyle thememode={thememode}
                    h={iconsH} w={iconsW}
                    fillColor={getTheme(thememode, "colorCrudHover")}
                    hoverColor={getTheme(thememode, "color1")}
                ><Setting
                        style={{
                            height: "100%", margin: 0, padding: 0
                        }} /></MiniButtonStyle>} />

        </Column>
    }
    return useMemo(() => render(), [props, thememode, driverTemplates])


}