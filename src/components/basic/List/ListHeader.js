import { v4 as uuid } from "uuid";
import { HeaderText, ListRowStyle } from "./style";
import { getTheme } from "../../../assets/theme"
import { useSelector } from "react-redux";
export default function ListHeader(props) {

    const thememode = useSelector(state => state.theme)

    return <ListRowStyle
        thememode={thememode}
        header
        shadow
        w={props.width}
        h={props.height / props.elementCount}
        borderRadius={"0px"}
        color={getTheme(thememode, "colorHeader")} key={uuid()}>
        {
            props.attributes?.map(e => <HeaderText
                thememode={thememode}
                textHori={props.textHori}
                fontRatio={props.fontRatio}
                w={props.uWidth}
                elementCount={props.elementCount}
                parentWidth={props.parentWidth}
                key={uuid()}>{e}</HeaderText>)
        }
        {props.readOnly == true ? <></> : <div style={{ width: "3%" }} />}
    </ListRowStyle>
}