import { v4 as uuid } from "uuid";
import { ListRowStyle, TitleText } from "./style";
import ListCrud from "./ListCrud";
import { useMemo } from "react";
import { useSelector } from "react-redux";
import { getTheme } from "../../../assets/theme";

export default function ListRow(props) {

    const thememode = useSelector(state => state.theme)
    function render() {
        return <ListRowStyle
            thememode={thememode}
            shadow key={uuid()}
            color={getTheme(thememode, "colorRow")}
            w={props.width}
            h={props.height / props.elementCount}
            parentWidth={props.parentWidth}
        >
            {
                props.attributes?.map(e => <TitleText
                    thememode={thememode}
                    textHori={props.textHori}
                    fontRatio={props.fontRatio}
                    w={props.uWidth}
                    elementCount={props.elementCount}
                    parentWidth={props.parentWidth}
                    key={uuid()}>{e ?? '?'}</TitleText>)
            }
            {props.comp}
            {
                props.readOnly == true ? <></> :
                    <ListCrud
                        parentWidth={props.parentWidth}
                        settingFn={props.settingFn}
                        data={props.data}
                        deleteFn={props.deleteFn}
                        _id={props._id} />}

        </ListRowStyle>
    }
    return useMemo(() => render(), [props, thememode])

}