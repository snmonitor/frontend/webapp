import ListRow from "./ListRow";
import ListHeader from "./ListHeader";
import { Logo, Text } from "./style";
import { Column } from "../../style";
import { v4 as uuid } from 'uuid';
import { useMemo } from "react";
import { useSelector } from "react-redux";

export default function List(props) {

    const thememode = useSelector(state => state.theme)

    const uWidth = (`${100 / (props.header?.length ?? 1)}%`)

    function render() {
        return <div style={{
            padding: `${props.style?.padding ?? "0 0 50px 0"}`,
            margin: `${props.style?.margin}`,
            overflowX: "hidden",
            overflowY: "auto",

        }}>

            <ListHeader
                fontRatio={props.fontRatio}
                width={"100%"}
                height={'20%'}
                uWidth={uWidth}
                elementCount={props.elementCount ?? 1}
                readOnly={props.readOnly}
                attributes={props.header}
                textHori={props.textHori}
                parentWidth={props.style?.width}
                headerLength={props.header?.length}
            />

            {props.rows?.map((e, index) => {
                return <ListRow key={uuid()} attributes={e}
                    fontRatio={props.fontRatio}
                    textHori={props.textHori}
                    width={"100%"}
                    height={'80%'}
                    uWidth={uWidth}
                    readOnly={props.readOnly}
                    parentWidth={props.style?.width}
                    elementCount={props.elementCount ?? 1}
                    deleteFn={props.deleteFn}
                    settingFn={props.settingFn}
                    _id={props._ids[index]}
                    data={props.rowsInfo?.length && props.rowsInfo[index]}
                    comp={
                        props.haveComponent ?? false == true ? props.component :
                            props.constructor ? <Column
                                verti={"center"}
                                style={{ margin: "5px 0 5px 0", width: `${uWidth}` }}>
                                <Text
                                    thememode={thememode}
                                    fontRatio={props.fontRatio}
                                    parentWidth={props.style?.width}
                                    elementCount={props.elementCount ?? 1}
                                >{props.constructor
                                    && props.constructor[index]?.name?.slice(0, 15)}</Text>
                                {props.constructor[index]?.logo && <Logo
                                    parentWidth={props.style?.width}
                                    elementCount={props.elementCount ?? 1}
                                    src={props.constructor[index]?.logo} />}</Column> : <></>} />
            })}</div>
    }

    return useMemo(() => render(), [props, thememode])



}