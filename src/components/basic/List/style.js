import styled from 'styled-components'
import { getTheme } from '../../../assets/theme'

export const ListRowStyle = styled.div`
  display: flex;
  align-items: center;
  justify-content: ${p => p.textHori ?? "space-between"};
  width: ${p => p.w};
  height:${p => p.h};
  margin: 0 0 6px 0;
  padding: ${props => props.padding ?? "5px 0 10px 0px"};
  background: ${props => props.color ?? getTheme(props.thememode, "colorHeader")};
  box-shadow: ${props => props.shadow && `2px 2px ${getTheme(props.thememode, "colorShadow")}`};
  border-radius: ${props => props.borderRadius ?? '5px'};
  color: ${props => props.header ? getTheme(props.thememode, "colorHeaderText") : getTheme(props.thememode, "color4")};

  &:hover {
    color: ${props => getTheme(props.thememode, "colorRowHoverText")};
    background-color: ${props => getTheme(props.thememode, "colorRowHover")};
  }
`




export const Text = styled.div`
  font-family: ${p => getTheme(p.thememode, "fontFamily")};
  color: inherit;

  @media screen and (min-width: 911px) {
    font-size:${p => p.parentWidth ? `calc(${p.parentWidth}/${p.elementCount}*${p.fontRatio * 0.5})` : "large"};
}

@media screen and (max-width: 910px) {
    font-size: ${p => p.parentWidth ? `calc(${p.parentWidth}/${p.elementCount}*${p.fontRatio * 0.5})` : "medium"};

}

@media screen and (max-width: 725px) {
    font-size:${p => p.parentWidth ? `calc(${p.parentWidth}/${p.elementCount}*${p.fontRatio * 0.5})` : "xx-small"};

}
`
export const Logo = styled.img`

  @media screen and (min-width: 911px) {
    height:30px;
}

@media screen and (max-width: 910px) {
      height:${p => p.parentWidth ? `calc(${p.parentWidth}*0.08)` : "30px"};
      max-width: 100px;
}

@media screen and (max-width: 725px) {
      height:${p => p.parentWidth ? `calc(${p.parentWidth}*0.08)` : "20px"};
      max-width: 100px;
}
`

export const HeaderText = styled.div`
  font-family: ${p => getTheme(p.thememode, "fontFamily")};
  color: inherit;
  display: flex;
  margin-top: 4px;
  padding-left:${p => p.textHori === "flex-start" ? "12px" : "0"};
  justify-content: ${p => p.textHori ?? "center"};
  width: ${p => p.w ?? "inherit"};
  font-size: ${p => p.parentWidth ? `calc(${p.parentWidth}/${p.elementCount}*${p.fontRatio * 0.7})` : "1.5vw"} !important;
`
export const TitleText = styled.div`
  font-family: ${p => getTheme(p.thememode, "fontFamily")};
  padding-left:${p => p.textHori === "flex-start" ? "12px" : "0"};
  color: inherit;
  display: flex;
  overflow-x: auto;
  justify-content: ${p => p.textHori ?? "center"};
  width: ${p => p.w ?? "inherit"};
  font-size: ${p => p.parentWidth ? `calc(${p.parentWidth}/${p.elementCount}*${p.fontRatio})` : 'xx-large'} ;


@media screen and (max-width: 910px) {
  font-size: ${p => p.parentWidth ? `calc(${p.parentWidth}/${p.elementCount}*${p.fontRatio})` : 'x-large'} ;
}

@media screen and (max-width: 725px) {
  font-size: ${p => p.parentWidth ? `calc(${p.parentWidth}/${p.elementCount}*${p.fontRatio})` : 'large'} ;
}
`

export const MiniButtonStyle = styled.button`
  && {
    width: ${p => p.parentWidth ? p.parentWidth : p.w ?? "35px"};
    height: ${p => p.parentWidth ? p.parentWidth : p.h ?? "35px"};
    border-radius: 5px;
    border-style: solid;
    border-width: 0;
    margin: 0.5px;
    padding: 5px;
    font-family: ${p => getTheme(p.thememode, "fontFamily")};
    font-size: large;
    background-color: transparent;
    fill: ${props => props.fillColor ?? getTheme(props.thememode, "colorError")};
    transition-duration: 0.2s;

    &:hover {
      background-color: transparent;
      fill: ${props => props.hoverColor ?? getTheme(props.thememode, "colorError")};
      cursor: pointer;
      transform: scale(1.2);
      transition-duration: 0.2s;
      transition-delay: 0s;
    }

    @media screen and (max-width: 900px) {
  transform:  ${p => p.parentWidth ? "scale(1)" : "scale(0.7)"};
}
@media screen and (max-width: 725px) {
  transform:  ${p => p.parentWidth ? "scale(1)" : "scale(0.5)"};
}


  }
`