import { getTheme } from "../../../assets/theme";
import { Accordion, AccordionSummary } from "@mui/material";
import * as React from "react";
import { useState } from "react";
import { Row } from "../../style";
import { ArrowDown, ArrowLeft } from "../../../assets/icons";
import { useSelector } from "react-redux";

export default function MoreDetails(props) {

    const thememode = useSelector(state => state.theme)
    const [open, setOpen] = useState(props.open ?? true)
    return <Accordion
        expanded={open}
        style={{
            backgroundColor: props.backColor ??
                getTheme(thememode, "color2"),
            margin: `${props.margin ?? "0px"}`,
        }}>
        <AccordionSummary
            onClick={() => setOpen(!open)}
            aria-controls="panel1a-content"
            id="panel1a-header"
        >
            <Row style={{
                width: "100%",
            }} hori={'space-between'}>
                <div style={{
                    color: `${props.labelColor ?? getTheme(thememode, "color0")}`,
                    fontSize: "large"
                }}>{props.title}</div>
                {open
                    ? <ArrowDown style={{ fill: `${props.labelColor}`, width: "10px" }} />
                    : <ArrowLeft style={{ fill: `${props.labelColor}`, width: "6px" }} />}
            </Row>
        </AccordionSummary>


        {props.children ?? <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.</div>}


    </Accordion>
}