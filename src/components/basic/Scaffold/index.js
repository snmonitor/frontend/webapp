import { CircularProgress, } from "@mui/material";
import { useEffect } from 'react'
import { Error, Info, Success, Warning } from "../../../assets/icons";
import { useTranslation } from "react-i18next";
import { errorException } from "../../../errors/errorException";
import { getTheme } from "../../../assets/theme";
import { showNotification, updateNotification } from "@mantine/notifications";
import { useSelector } from "react-redux";
export default function Scaffold(props) {

    const thememode = useSelector(state => state.theme)
    const [translate] = useTranslation('common');
    const updateNotif = (title, message, icon,
    ) => updateNotification({
        id: 'notif',
        autoClose: 5000,
        title: title,
        message: message,
        icon: icon,
        loading: false
    });

    const showNotif = (title, message,) => showNotification({
        id: 'notif',
        title: title,
        message: message,

        loading: true,
        autoClose: false,
    });
    const closeNotif = (title, message,) => updateNotification({
        id: 'notif',
        title: title,
        message: message,

        loading: true,
        autoClose: 200,
    });

    const severity = {
        loading: { name: "loading", color: "#d3f2ff", message: `${translate("common.loading")}...`, icon: <CircularProgress style={{ height: "40px", width: "40px", padding: "10px" }} /> },
        info: {
            color: "#d3f2ff", iconColor: `${getTheme(thememode, "colorInfo")}`,
            icon: <Info style={{
                background: "white",
                fill: `${getTheme(thememode, "colorInfo")}`, height: "30px"
            }} />
        },
        error: {
            color: "#fce0e0", iconColor: `${getTheme(thememode, "colorError")}`,
            icon: <Error style={{
                background: "white",
                fill: `${getTheme(thememode, "colorError")}`, width: "50px", height: "50px"
            }} />
        },
        warn: {
            color: "#ffeed8", iconColor: `${getTheme(thememode, "colorWarning")}`,
            icon: <Warning style={{
                background: "white",
                fill: `${getTheme(thememode, "colorWarning")}`, height: "50px"
            }} />
        },
        success: {
            color: "#d8ffd8", iconColor: `${getTheme(thememode, "colorSuccess")}`,
            icon: <Success style={{
                background: "white",
                fill: `${getTheme(thememode, "colorSuccess")}`, height: "50px"
            }} />
        },
    }





    useEffect(() => {
        if (props.error && props.error.code) {
            const response = errorException(translate, props.error.data, props.error.param, props.error.message)[props.error.code ?? 10]

            if (response.status >= 300) {

                updateNotif(response.status, response.message, severity.error.icon,
                    'black', severity.error.color)


            } else if (response.status < 300) {
                updateNotif(response.status, response.message, severity.success.icon,
                    'black', severity.success.color)
            }


        } else if (props.error.loading === true) {
            showNotif("loading...", " ",
                severity.loading.color)
        }
        else
            closeNotif("loading...", " ",
                severity.loading.color)


    }, [props.error])





    function render() {
        //success
        return <div style={{
            backgroundColor: getTheme(thememode, "colorBg"),
            display: "flex", flexDirection: "column",
            width: '100vw', height: "100vh"
        }}>
            <div>{props.children}</div></div>
    }

    return render()
}