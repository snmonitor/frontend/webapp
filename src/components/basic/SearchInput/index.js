import * as React from 'react';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import { History, Search } from "../../../assets/icons";
import { InputBase } from "@mui/material";
import { SearchRow } from "./style";
import { getTheme } from "../../../assets/theme";
import { useSelector } from 'react-redux';

export default function SearchInput(props) {

    const thememode = useSelector(state => state.theme)
    return <SearchRow style={props.style}
        thememode={thememode}>
        <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder={props.placeholder}
            type={"search"}
            onKeyPress={(e) => {
                if (e.key === 'Enter') {
                    e.preventDefault()
                    props.setValue(e.target.value)
                }
            }}
            onBlur={(e) => {
                e.preventDefault()
                props.setValue(e.target.value)
            }}
        />
        <IconButton type="submit" sx={{
            p: '10px',
            fill: `${getTheme(thememode, "color4")}`
        }} aria-label="search">
            <Search style={{ height: "20px", width: "20px", }} />
        </IconButton>
        <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
        <IconButton color="primary" sx={{
            p: '10px',
            fill: `${getTheme(thememode, "color4")}`
        }} aria-label="directions">
            <History style={{ height: "20px", width: "20px", }} />
        </IconButton>
    </SearchRow>
}