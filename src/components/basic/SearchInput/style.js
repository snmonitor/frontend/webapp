import styled from 'styled-components'
import { getTheme } from '../../../assets/theme';
import { Row } from "../../style";

export const SearchRow = styled(Row)`
  && {
    display: flex;
    align-items: center;
    width: 100%;
    transition-duration: 0.4s;
    background-color: ${props => getTheme(props.thememode, "color2")};
    box-shadow: 2px 2px ${props => getTheme(props.thememode, "colorShadow")};
  }
`
