import { useEffect, useState } from "react";
import { pages } from "../../../enums";
import { Body } from "./style";
import { MaterialUISwitch, MiniText, } from '../../style'
import { logoutAction } from "../../../redux/auth/auth_action";
import { useDispatch, useSelector } from "react-redux";
import { Column } from "../../style";
import { v4 as uuid } from 'uuid'
import { Drawer, LabeledButton } from "../../";
import { Analytique, Driver, Home, Logout, Profile, Scan } from '../../../assets/icons'
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";
import { Badge } from "@mui/material";
import { darkAction, lightAction } from "../../../redux/theme/theme_action";
import { getTheme } from "../../../assets/theme";
export default function SideBar(props) {

    const thememode = useSelector(state => state.theme)

    const dispatch = useDispatch()
    const [actualPage, setActualPage] = useState(pages.Dashboard)
    const [menu, setMenu] = useState(false);
    const [translate] = useTranslation('common');
    const location = useLocation()
    const [severityError, setSeverityError] = useState(0)
    const kpi = (useSelector(state => state.kpi?.real) ?? [{ severity1: 0 }])

    let path = location.pathname.slice(1)

    const iconHeight = "25px"
    const iconWidth = "25px"

    useEffect(() => {

        setSeverityError(
            kpi?.reduce(
                (previous, next) => {
                    return parseInt(previous ?? 0) + parseInt(next?.severity1 ?? 0)
                }, 0))
    }, [kpi])

    useEffect(() => {
        if (path !== actualPage && path !== pages.Login)
            setActualPage(path)
        if (path == pages.Dashboard)
            setSeverityError(0)
    }, [location])

    function profileClicked(event) {

        if (
            event &&
            event.type === 'keydown' &&
            (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setMenu(true);
    }


    return <><Body thememode={thememode}>

        <LabeledButton
            to={`/${pages.Dashboard}`}>
            <img src="favicon.png" style={{ height: "50px" }} />
            <MiniText
                thememode={thememode}
                style={{
                    padding: '3px',
                    color: getTheme(thememode, "color4")
                }}>sn-monitor</MiniText>

        </LabeledButton>

        <LabeledButton key={uuid()}
            to={`#`}
            handleClick={(e) => profileClicked(e)}>
            <Profile style={{ height: iconHeight, width: iconWidth, }} />
            <MiniText
                thememode={thememode}
                style={{ padding: '3px' }}>{translate('profile_page.title')}</MiniText>

        </LabeledButton>
        <Drawer pos={'right'}
            logout={<LabeledButton key={uuid()}
                to={`#`}
                handleClick={() => {
                    dispatch(logoutAction())
                }}>
                <Logout style={{ height: iconHeight, width: iconWidth, }} />
                <MiniText
                    thememode={thememode}
                    style={{ padding: '3px' }}>{translate('logout')}</MiniText>
            </LabeledButton>}
            menu={menu}
            setMenu={setMenu}
            username={props.user?.username ?? props.user?.name}
            email={props.user?.email}
            role={props.user?.role}
        />
        <Column>

            <LabeledButton to={`/${pages.Dashboard}`}
                focused={(actualPage === pages.Dashboard).toString()}
                handleClick={() => {
                    setActualPage(pages.Dashboard)
                }}>
                <Badge badgeContent={path == pages.Dashboard ? 0 : severityError} color="error">
                    <Home style={{ height: iconHeight, width: iconWidth, }} /></Badge>
                <MiniText
                    thememode={thememode}
                    style={{ padding: '3px' }}>{translate('dashboard_page.title')}</MiniText>

            </LabeledButton>

            <LabeledButton to={`/${pages.Analytique}`}
                focused={(actualPage === pages.Analytique).toString()}
                handleClick={() => {
                    setActualPage(pages.Analytique)
                }}>
                <Analytique style={{ height: iconHeight, width: iconWidth, }} />
                <MiniText
                    thememode={thememode}
                    style={{ padding: '3px' }}>{translate('analytic_page.title')}</MiniText>
            </LabeledButton>

            <LabeledButton to={`/${pages.Scan}`}
                focused={(actualPage === pages.Scan).toString()}
                handleClick={() => {
                    setActualPage(pages.Scan)
                }}>
                <Scan style={{ height: iconHeight, width: iconWidth, }} />
                <MiniText
                    thememode={thememode}
                    style={{ padding: '3px' }}>{translate('scan_page.title')}</MiniText>
            </LabeledButton>



            <LabeledButton to={`/${pages.Driver}`}
                focused={(actualPage === pages.Driver).toString()}
                handleClick={() => {
                    setActualPage(pages.Driver)
                }}>
                <Driver style={{ height: iconHeight, width: iconWidth, }} />
                <MiniText
                    thememode={thememode}
                    style={{ padding: '3px' }}>{translate('drivers_page.title')}</MiniText>
            </LabeledButton>
        </Column>
        <div />

        <MaterialUISwitch sx={{ m: 1 }}

            checked={thememode === "DARK"}
            value={thememode}
            onClick={(e) => {
                e.target.value === "LIGHT"
                    ? dispatch(darkAction())
                    : dispatch(lightAction())



            }
            }
        />


    </Body>
        <div style={{ height: "100vh", minWidth: "81px" }} />
    </>
}