import styled from 'styled-components'
import { Link } from "react-router-dom";
import { getTheme } from '../../../assets/theme';


export const LinkText = styled(Link)`
  text-decoration: none;
  font-family: ${p => getTheme(p.thememode, "fontFamily")};
  &:active {
    color: aqua;
  }
`


export const Body = styled.div`
  background: ${props => getTheme(props.thememode, "color2")} linear-gradient(${props => getTheme(props.thememode, "color1")}, transparent);
  height: 100vh;
  z-index: 1;
  display: flex;
  position: fixed;
  justify-content: space-around;
  align-items: center;
  flex-direction: column;

`