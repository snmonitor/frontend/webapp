import * as React from 'react';
import { styled } from '@mui/material/styles';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { getTheme } from '../../../assets/theme';
import { Box } from '@mui/material';
import { useSelector } from 'react-redux';


const StyledTabs = styled((props) => (
  <Tabs
    {...props}
    TabIndicatorProps={{
      children: <span className="MuiTabs-indicatorSpan" />
    }}

  />
))({
  '& .MuiTabs-indicator': {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  '& .MuiTabs-indicatorSpan': {
    maxWidth: 40,
    width: '100%',
    backgroundColor: `${p => getTheme(p.thememode, "color4")}`,
  },
});

const StyledTab = styled((props) =>
  <Tab disableRipple {...props}

    style={{
      width: "14vw", borderRight: `${props.index != 3 ? `solid 1px ${getTheme(props.thememode, "color4")}` : "0px"}`,
    }} />)(
      ({ theme, thememode }) => ({

        '&.Mui-selected': {
          color: `${getTheme(thememode, "color5")}`,
        },


        color: `${getTheme(thememode, "color3")}`,


        textTransform: 'none',
        fontWeight: theme.typography.fontWeightRegular,
        fontSize: theme.typography.pxToRem(15),
        marginRight: theme.spacing(1),
      }),
    );


export default function Tabss(props) {
  const thememode = useSelector(state => state.theme)
  const handleChange = (event, newValue) => {
    if (props.setValue)
      props.setValue(newValue);
  };

  return (
    <Box sx={{
      p: 1, backgroundColor: `${getTheme(thememode, "color1")}`,
      height: `${props.height} `,
      display: 'flex',
      justifyContent: "center"
    }}>
      <StyledTabs

        value={props.value ?? 0}
        onChange={handleChange}

        aria-label="styled Tabs example"
      >
        {props.labels?.map((e, index) => {
          return <StyledTab
            thememode={thememode}
            disabled={!props.enabled}
            key={index} index={index} label={e} />

        })}

      </StyledTabs>

    </Box >

  );
}
