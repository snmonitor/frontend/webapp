import * as React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from '@mui/material/SvgIcon';
import { alpha, styled } from '@mui/material/styles';
import TreeView from '@mui/lab/TreeView';
import TreeItem, { treeItemClasses } from '@mui/lab/TreeItem';
import Collapse from '@mui/material/Collapse';
import { useState, useEffect } from "react"
// web.cjs is required for IE11 support
import { useSpring, animated } from '@react-spring/web';
import { v4 as uuid } from 'uuid'
import { getTheme } from '../../../assets/theme';
import { Error, Success, Warning } from '../../../assets/icons';
import { Row } from '../../style';
import { isIterable, randomString } from '../../../Helper';
import { CircularProgress } from '@mui/material';
import { Center } from '../Center';
import { useSelector } from 'react-redux';


function MinusSquare(props) {
  return (
    <SvgIcon fontSize="inherit" style={{ width: 14, height: 14 }} {...props}>
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 11.023h-11.826q-.375 0-.669.281t-.294.682v0q0 .401.294 .682t.669.281h11.826q.375 0 .669-.281t.294-.682v0q0-.401-.294-.682t-.669-.281z" />
    </SvgIcon>
  );
}

function PlusSquare(props) {
  return (
    <SvgIcon fontSize="inherit" style={{ width: 14, height: 14 }} {...props}>
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 12.977h-4.923v4.896q0 .401-.281.682t-.682.281v0q-.375 0-.669-.281t-.294-.682v-4.896h-4.923q-.401 0-.682-.294t-.281-.669v0q0-.401.281-.682t.682-.281h4.923v-4.896q0-.401.294-.682t.669-.281v0q.401 0 .682.281t.281.682v4.896h4.923q.401 0 .682.281t.281.682v0q0 .375-.281.669t-.682.294z" />
    </SvgIcon>
  );
}

function CloseSquare(props) {
  return (
    <SvgIcon
      className="close"
      fontSize="inherit"
      style={{ width: 14, height: 14 }}
      {...props}
    >
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M17.485 17.512q-.281.281-.682.281t-.696-.268l-4.12-4.147-4.12 4.147q-.294.268-.696.268t-.682-.281-.281-.682.294-.669l4.12-4.147-4.12-4.147q-.294-.268-.294-.669t.281-.682.682-.281.696 .268l4.12 4.147 4.12-4.147q.294-.268.696-.268t.682.281 .281.669-.294.682l-4.12 4.147 4.12 4.147q.294.268 .294.669t-.281.682zM22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0z" />
    </SvgIcon>
  );
}

function TransitionComponent(props) {
  const style = useSpring({
    from: {
      opacity: 0,
      transform: 'translate3d(20px,0,0)',
    },
    to: {
      opacity: props.in ? 1 : 0,
      transform: `translate3d(${props.in ? 0 : 20}px,0,0)`,
    },
  });

  return (
    <animated.div style={style}>
      <Collapse {...props} />
    </animated.div>
  );
}

TransitionComponent.propTypes = {
  /**
   * Show the component; triggers the enter or exit states
   */
  in: PropTypes.bool,
};

const StyledTreeItem = styled((props) => (
  <TreeItem
    sx={{ color: `${getTheme(props.thememode, "color4")}` }}
    {...props}
    TransitionComponent={TransitionComponent} />
))(({ theme }) => ({
  width: "12px",
  [`& .${treeItemClasses.iconContainer}`]: {
    '& .close': {
      opacity: 0.3,
    },
  },
  [`& .${treeItemClasses.group}`]: {
    marginLeft: 15,
    paddingLeft: 18,
    borderLeft: `1px dashed ${alpha(theme.palette.text.primary, 0.4)}`,
  },
}));




function label(text = randomString(Math.random() * 20), kpi, thememode) {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  context.font = "large karla";
  const width = context.measureText(text).width + 25
  return <Row hori={"space-between"} style={{ width: `${width}px`, }}>
    <div style={{
      whiteSpace: "nowrap",
      font: `${context.font}`
    }}>{text}</div>
    {kpi?.severity1 > 0 ? <Error style={{
      height: "15px",
      fill: `${getTheme(thememode, "colorError")}`
    }} />
      : kpi?.severity2 > 0 ? <Warning style={{
        height: "15px",
        fill: `${getTheme(thememode, "colorWarning")}`
      }} />
        : <Success style={{
          height: "15px",
          fill: `${getTheme(thememode, "colorSuccess")}`
        }} />}   </Row>
}



export default function Tree(props) {

  const thememode = useSelector(state => state.theme)
  const kpi = useSelector(state => state.kpi.real)
  const [filteredElement, setFilteredElement] = useState(props.elements)
  const [selectedElement, setSelectedElement] = useState(isIterable(props.elements) && props.elements[0])
  useEffect(() => {
    filterElements(props.searchValue ?? '')
    setSelectedElement(isIterable(props.elements) && props.elements[0])
  }, [props.searchValue, props.elements])

  useEffect(() => {
    if (props.setSelectedElement)
      props?.setSelectedElement(selectedElement)
  }, [selectedElement])

  return (

    filteredElement ?

      <TreeView
        aria-label="customized"
        defaultExpanded={['1']}
        defaultCollapseIcon={<MinusSquare />}
        defaultExpandIcon={<PlusSquare />}
        defaultEndIcon={<CloseSquare />}
        sx={{
          padding: '2px',
          flexGrow: 1,
          boxShadow: `2px 2px ${getTheme(thememode, "colorShadow")}`,
          border: "solid 0px",
          overflowY: 'auto', ...props.style
        }}>
        {filteredElement?.map((e, index) => {
          return <StyledTreeItem
            thememode={thememode}
            style={{
              color: `${props.setSelectedElement ?
                e?._id == selectedElement?._id ?
                  getTheme(thememode, "color4") : getTheme(thememode, "color6")
                : getTheme(thememode, "color4")}`
            }}
            key={index} nodeId={uuid()} label={label(e[props.valueKey], kpi?.filter(el =>
              el.driverId == e._id)[0], thememode)}
            onClick={() => setSelectedElement(e)} />

        }
        )}

      </TreeView >

      : <Center height={"210px"}> <CircularProgress
        style={{ color: `${getTheme(thememode, "color4")}` }} /></Center >
  );

  function filterElements(filter) {
    if (props.elements?.length) {
      setFilteredElement(props.elements?.filter(e => {
        return e[props.valueKey]?.toUpperCase().includes(filter.toUpperCase())
      }))
    }
    else
      setFilteredElement(props.elements)
  }
}
