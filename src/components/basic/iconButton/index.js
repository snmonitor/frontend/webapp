import { useSelector } from "react-redux";
import { v4 as uuid } from "uuid";
import { Column, IconButtonStyle } from "../../style";
import { LinkText } from "../SideBar/style";

export default function LabeledButton(props) {

    const thememode = useSelector(state => state.theme)
    return (
        <Column key={uuid()}> <LinkText
            thememode={thememode}
            key={uuid()} to={props.to ?? "#"}>
            <IconButtonStyle
                thememode={thememode}
                key={uuid()}
                focused={props.focused}
                onClick={props.handleClick}>
                {props.children}
            </IconButtonStyle> </LinkText>

        </Column>)
}