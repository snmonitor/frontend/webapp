import * as React from 'react';
import { Fragment } from 'react';
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import { Container, } from "./style";
import { En, Fr, User } from "../../../assets/icons";
import { Column, Row } from "../../style";
import { useTranslation } from "react-i18next";
import { useSelector } from 'react-redux';
import LabeledButton from '../../basic/iconButton';
import { getTheme } from '../../../assets/theme';


export default function Drawer(props) {

    const thememode = useSelector(state => state.theme)
    const { i18n } = useTranslation('common');

    async function changeLanguageTo(lang) {
        await i18n.changeLanguage(lang)
    }

    return (
        <div>
            <Fragment key={props.pos}>
                <SwipeableDrawer
                    anchor={props.pos}
                    open={
                        props.menu
                    }
                    onClose={() => {
                        props.setMenu(false)
                    }}
                    onOpen={() => {
                        props.setMenu(false)
                    }}>
                    <Container thememode={thememode}>
                        <Column
                            hori="space-between"
                            verti="space-around"
                            width="100%" height="100%">
                            <Row style={{ padding: "30px" }} hori={"space-around"}>
                                <Column>
                                    <User style={{ width: '5vw', fill: getTheme(thememode, "color4") }} />
                                    <div style={{ paddingTop: "10px", color: getTheme(thememode, "color4") }}>{props.role}</div>
                                </Column>
                                <Column height={"5vw"} verti={"space-between"} hori={"center"}>
                                    <div style={{ color: getTheme(thememode, "color4") }}>{props.username}</div>
                                    <div style={{ color: getTheme(thememode, "color4") }}>{props.email}</div>
                                    <div />
                                </Column>
                            </Row>
                            <Row>
                                <LabeledButton style={{ margin: "3px" }}
                                    handleClick={() => changeLanguageTo('fr')}>
                                    <Fr style={{ height: "25px" }} />
                                    <div style={{ fontSize: "large" }}>FR</div>
                                </LabeledButton >

                                <LabeledButton style={{ margin: "3px" }}
                                    handleClick={() => changeLanguageTo('en')}>
                                    <En style={{ height: "25px" }} />
                                    <div style={{ fontSize: "large" }}>EN</div></LabeledButton >
                            </Row>

                            {props.logout}
                        </Column>
                    </Container>

                </SwipeableDrawer>
            </Fragment>

        </div>
    );
}