import styled from 'styled-components'
import { getTheme } from '../../../assets/theme'


export const Container = styled.div`
  background: ${props => getTheme(props.thememode, "color1")};
  width: 30vw;
  height: 100vh;
`


