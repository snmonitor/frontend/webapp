import { v4 as uuid } from "uuid";
import { Grid } from "@mui/material";
import { InputInfo } from "../../.";
import { useSelector } from "react-redux";
import { getTheme } from "../../../assets/theme";
import InputSelect from "../../basic/Input/InputSelect"
import { useEffect, useState } from "react";
import { Column, MiniText } from "../../style";
import { isIterable } from "../../../Helper";

export default function InfoBar(props) {

     const thememode = useSelector(state => state.theme)
     const userCount = useSelector(state => state.user?.count)
     const { donne: drivers, pagination } = useSelector(state => state.driver)
     const [selectedDriverID, setSelectedDriverID] = useState('')

     useEffect(() => {
          setSelectedDriverID(drivers ? drivers[0]?._id : "none")
     }, [drivers])

     useEffect(() => {
          if (props.setSelected && isIterable(drivers))
               props?.setSelected(...drivers.filter(e => e._id == selectedDriverID))
     }, [selectedDriverID])


     return (<Column verti="flex-start" hori="center" style={{
          width: "20vw",
          position: "absolute", right: 0, top: '0',

     }}>
          <Grid container
               justifyContent="center"
               alignItems="flex-start"
               columnSpacing={1}
               rowSpacing={1}
               columns={{ xs: 6, md: 12 }}
               style={{ width: "210px", }}>

               {[{
                    label: "connected user",
                    data: userCount ?? 1
               }, {
                    label: "Sensors",
                    data: pagination?.total
               },]?.map((e) => {
                    return <Grid key={uuid()} item xs={6}><InputInfo key={uuid()}
                         label={e.label}
                         data={e.data}
                    ></InputInfo></Grid>
               })}
               <Grid item xs={12}>
                    <MiniText

                         thememode={thememode}
                         style={{
                              color: getTheme(thememode, "color4"),
                              whiteSpace: 'nowrap',
                              overflow: 'hidden'
                         }}
                    >Drivers</MiniText>

                    <InputSelect
                         setValue={setSelectedDriverID}
                         default={selectedDriverID ?? "none"}
                         style={{
                              color: getTheme(thememode, "color4"),
                              background: getTheme(thememode, "color1"),
                         }}
                         values={drivers?.map(e => { return { value: e._id, name: e.name } })} />
               </Grid>
          </Grid></Column >)
}