import { Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector, } from "react-redux";
import { List } from "../../index";
import { getTheme } from "../../../assets/theme";
import { isIterable, isNullEmptyOrWhiteSpace } from "../../../Helper";
import { Row, Title } from "../../style";

export default function ListPage(props) {

    const thememode = useSelector(state => state.theme)
    const elementPerPage = props.elementPerPage;
    const dispatch = useDispatch()

    //get tous les initial element
    useEffect(() => {
        if (props.getAllElements)
            dispatch(props.getAllElements(elementPerPage * (page - 1), elementPerPage))
    }, [])


    const [pag, setPag] = useState(props.pagination)
    const [page, setPage] = useState(1)
    const [rows, setRows] = useState(elementsToArray(props.elements))
    const [ids, setIds] = useState(elementsIds(props.elements))
    const [filteredElement, setFilteredElement] = useState(props.elements)
    const [constructorKeys, setConstructorKeys] = useState(constructorsToArray(props.constructor == true ? props.elements : []))

    function updatePage(limit = elementPerPage, skip = elementPerPage * (page - 1), total) {

        const count = total ?? props.pagination?.total ?? 0;
        const totalPages = limit === 0 ? 1 : Math.ceil(count / limit) ?? 1
        const currentPage = skip === 0 ? 1 : Math.ceil(totalPages % skip) ?? 1

        setPag({
            "total": count,
            "currentpage": currentPage,
            "pages": totalPages
        })
    }

    useEffect(() => {
        updatePage()
    }, [props.pagination])

    //swith to the first page if user is deleted
    useEffect(() => {
        if ((page - 1) * elementPerPage === pag?.total && page > 1)
            pageChanged('', 1)
    }, [pag])

    useEffect(() => {
        filterElements(props.searchValue)
    }, [props.searchValue, props.elements, props.keys])

    useEffect(() => {
        setRows(elementsToArray(filteredElement))
        setIds(elementsIds(filteredElement))
        if (props.constructor == true)
            setConstructorKeys(constructorsToArray(filteredElement))
        if (!isNullEmptyOrWhiteSpace(props.searchValue))
            //limit,skip,search input length
            updatePage(elementPerPage, elementPerPage * (page - 1), filteredElement?.length)
        else
            updatePage()
    }, [filteredElement])


    function pageChanged(e, v) {
        setPage(v)
        if (props.getAllElements)
            dispatch(props.getAllElements(elementPerPage * (v - 1), elementPerPage))
    }


    function filterElements(filter = '') {
        if (props.elements?.length) {
            setFilteredElement(props.elements?.filter(e => {
                return props.keys.filter((key) => {
                    if (isIterable(key)) {
                        if (key.length > 2)
                            return e[key[0]][key[1]][key[2]]?.toString()?.toUpperCase().includes(filter.toUpperCase())
                        else return e[key[0]][key[1]]?.toString()?.toUpperCase()?.includes(filter.toUpperCase())

                    }
                    return e[key]?.toString()?.toUpperCase()?.includes(filter.toUpperCase())
                }).length > 0
            }))
        }
        else
            setFilteredElement(props.elements)
    }

    function elementsToArray(elements) {
        if (elements?.length) {

            return elements?.map(e =>
                props.keys.map((key) => {
                    if (isIterable(key))
                        return e[key[0]][key[1]]
                    if (key === "createdAt")
                        return new Date(e[key])?.toUTCString()
                    else
                        return e[key]
                })
            )

        }
    }

    function elementsIds(elements) {
        if (elements?.length)
            return elements?.map(e => {
                return e._id
            })
    }
    function constructorsToArray(elements) {
        if (elements?.length && props.constructor == true)
            return elements?.map(e => {
                return {
                    name: e.manufacturer?.name,
                    logo: e.manufacturer?.logo
                }
            })
    }


    function renderPagination() {
        if (elementPerPage < props.pagination?.total && props.withPage == true)
            return <Row style={{
                width: props.style?.width ?? "inherit",
                minWidth: props.style?.minWidth,
                borderTop: `2px solid ${getTheme(thememode, "color4")}`,
                ...props.paginationStyle
            }}>
                <Pagination style={{ margin: "10px 0 10px 0" }} page={page}
                    onChange={pageChanged} shape="rounded" size={`${props.paginationSize}`} count={pag?.pages} showFirstButton showLastButton />
            </Row>
    }

    function render() {
        return <div style={props.style}>
            <Row
                verti={"center"}
                hori={"space-between"}>
                {props.title && <Title
                    thememode={thememode}
                    fontRatio={props.fontRatio}
                    parentWidth={props.style?.width}
                    elementCount={props.headers?.length}

                    style={{
                        marginLeft: "5vw", cursor: "pointer",
                    }}>{props.title}</Title>}
                {props.action ?? <div />}
            </Row>
            <List
                //text hori when flex-start adding left padding
                textHori={props.textHori}
                fontRatio={props.fontRatio}
                readOnly={props.readOnly}
                style={props.style}
                settingFn={props.submit}
                deleteFn={props.delete}
                header={props.headers}
                elementCount={props.headers?.length}
                _ids={ids}
                rows={rows}
                rowsInfo={filteredElement}
                haveComponent={props.haveComponent}
                component={props.component}
                constructor={props.constructor == true && constructorKeys} />

            {renderPagination()}
        </div>
    }

    return render()

}