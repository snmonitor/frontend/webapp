import * as React from 'react';
import { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { FilledInput, FormControl, Icon, IconButton, InputAdornment, InputLabel, } from "@mui/material";
import { Email, Visibility, VisibilityOff } from "@mui/icons-material";
import LockOpenIcon from '@mui/icons-material/LockOpen';
import { getTheme } from "../../../assets/theme";
import { useTranslation } from "react-i18next";
import { useSelector } from 'react-redux';


function Copyright(pro) {
    const [translate] = useTranslation('common');

    return (
        <Typography variant="body2" color="text.secondary" align="center" {...pro}>
            {'Copyright © '}
            <Link color="inherit" href="https://www.google.fr/">
                {translate('login_page.your_website')}
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const theme = createTheme();

export default function Login(props) {
    const thememode = useSelector(state => state.theme)
    const [translate] = useTranslation('common');

    const [rememberMe, setRememberMe] = useState(false);
    const [showPassword, setShowPassword] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        props.submitLogin(data.get('email'), data.get('password'), rememberMe)
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh' }}>
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    sx={{
                        backgroundImage: props.backgroundImage ?? 'url(login3.jpg)',
                        backgroundRepeat: 'no-repeat',
                        backgroundColor: (t) =>
                            t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                    }}
                />
                <Grid item xs={12} sm={8} md={5} sx={{
                    backgroundColor: props.backgroundColor ?? '#F0E9D2',
                }} component={Paper} elevation={6} square>
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{
                            m: 1,
                            color: getTheme(thememode, "color4"),
                            backgroundColor: props.iconColor ??
                                getTheme(thememode, "color1")
                        }}>
                            <LockOpenIcon />
                        </Avatar>
                        <Typography component="h1" variant="h6">
                            {translate('login_page.title')}
                        </Typography>

                        <Box component="form" noValidate onSubmit={handleSubmit} sx={{
                            mt: 1,
                        }}>
                            <FormControl sx={{
                                m: 1, width: '100%',
                            }} variant="filled">
                                <InputLabel htmlFor="filled-email"
                                    sx={{
                                        color:
                                            getTheme(thememode, "color3"),
                                    }}
                                    error={props.auth.warning}
                                >{translate('login_page.email')}
                                </InputLabel>
                                <FilledInput
                                    inputProps={{
                                        style: {
                                            backgroundColor:
                                                getTheme(thememode, "color1"),
                                            color:
                                                getTheme(thememode, "color4")
                                        }
                                    }}
                                    required
                                    fullWidth
                                    name="email"
                                    id="email"
                                    autoComplete="email"
                                    error={props.auth.warning}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <Icon
                                                edge="end"
                                            >
                                                <Email />
                                            </Icon>
                                        </InputAdornment>
                                    }

                                />
                            </FormControl>
                            <FormControl sx={{ m: 1, width: '100%' }} variant="filled">
                                <InputLabel htmlFor="filled-adornment-password"
                                    error={props.auth.warning} sx={{
                                        color:
                                            getTheme(thememode, "color3"),
                                    }}
                                >{translate('login_page.password')}</InputLabel>
                                <FilledInput
                                    inputProps={{
                                        style: {
                                            backgroundColor:
                                                getTheme(thememode, "color1"),
                                            color:
                                                getTheme(thememode, "color4")
                                        }
                                    }}
                                    required
                                    fullWidth
                                    name="password"
                                    type={showPassword ? 'text' : 'password'}
                                    id="password"
                                    autoComplete="current-password"
                                    error={props.auth.warning}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                edge="end"
                                            >
                                                {showPassword ? <VisibilityOff /> : <Visibility />}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                />
                            </FormControl>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        style={{ color: getTheme(thememode, "color3") }}
                                        onClick={() => setRememberMe(!rememberMe)}
                                        checked={rememberMe}
                                        value="remember"
                                    />}
                                label={translate('login_page.remember_me')}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{
                                    mt: 3, mb: 2,
                                    backgroundColor: props.color ??
                                        getTheme(thememode, "color1"),
                                    color:
                                        getTheme(thememode, "color4"),
                                    '&:hover': {
                                        backgroundColor: props.color ??
                                            getTheme(thememode, "color2"),
                                    }
                                }
                                } >
                                {translate('login_page.login')}
                            </Button>
                            <Grid container>
                                <Grid item xs>
                                    <Link href="#" variant="body2"
                                        sx={{
                                            mt: 3, mb: 2,
                                            color: "#000000",
                                        }}>
                                        {translate('login_page.forgot_password')}
                                    </Link>
                                </Grid>

                            </Grid>
                            <Copyright sx={{ mt: 5 }} />
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </ThemeProvider >
    );
}