import { Dialog } from "@mui/material";
import { ButtonStyle, Row } from "../../style";
import * as React from 'react'
import { useState } from 'react'
import { Chrono, Close, Delay, Ethernet, Max, Network, Play, Reset, Retry } from "../../../assets/icons";
import { Container } from "./style";
import { useTranslation } from "react-i18next";
import { InputSelect, InputText, getTheme } from "../../index";
import MoreDetails from "../../basic/MoreDetails";
import { MiniButtonStyle, Text } from "../../basic/List/style";
import { v4 as uuid } from 'uuid'
import { useSelector } from "react-redux";

export default function ScanCard(props) {
    const thememode = useSelector(state => state.theme)
    const mainColor = "#F1F0EA"
    const parentDetailsColor = getTheme(thememode, "color3")
    const detailsColor = "#F1F0EA"
    const [translate] = useTranslation('common')
    const [open, setOpen] = useState(false)
    // fields
    const [driverTemplateID, setDriverTemplateID] = useState(props.data?.templateID)
    const defaultIP = props.data?.IP ?? ''
    const [ip, setIp] = useState(defaultIP)
    const defaultPort = props.data?.port ?? 161
    const [port, setPort] = useState(defaultPort)
    const defaultVersion = props.data?.version?.protocol ?? "SNMP v2"
    const [version, setVersion] = useState(defaultVersion)
    const defaultRetries = props.data?.retries ?? 1
    const [retries, setRetries] = useState(defaultRetries)
    const defaultTimeout = props.data?.timeout ?? 5000
    const [timeout, setTimeout] = useState(defaultTimeout)
    const defaultMaxOid = props.data?.maxOid ?? 50
    const [maxOid, setMaxOid] = useState(defaultMaxOid)
    const defaultDelay = props.data?.delay ?? 100
    const [delay, setDelay] = useState(defaultDelay)

    function closeDialog() {
        setOpen(false)
    }

    function resetField() {
        setIp(defaultIP)
        setPort(defaultPort)
        setVersion(defaultVersion)
        setRetries(defaultRetries)
        setTimeout(defaultTimeout)
        setMaxOid(defaultMaxOid)
        setDelay(defaultDelay)
    }
    return <>
        <div
            style={props.style}
            onClick={() => setOpen(true)}>
            {props.comp}
        </div>
        <Dialog
            maxWidth={"xl"}
            open={open}
            onClose={() => {
                setOpen(false)
            }}>

            <Container backColor={mainColor}>
                <Row hori={"space-between"}>
                    <Text
                        thememode={thememode}>{translate("scan_page.network_scan")}</Text>
                    <MiniButtonStyle thememode={thememode}
                        onClick={() => closeDialog()}
                        fillColor={getTheme(thememode, "color3")}
                    ><Close
                            style={{ height: "17px", margin: 0, padding: 0 }} /></MiniButtonStyle>
                </Row>
                <form onSubmit={async (e) => {
                    e.preventDefault()
                    resetField()
                    closeDialog()
                    const selectedTemplated = props.driverTemplates.find(e => e._id === driverTemplateID)
                    await props.submitFn(
                        {
                            "name": selectedTemplated?.name,
                            "manufacturer": selectedTemplated?.manufacturer,
                            "IP": ip,
                            "templateID": selectedTemplated._id,
                            "protocol": version,
                            "retries": retries,
                            "timeout": timeout,
                            "port": port,
                            "versions": selectedTemplated.versions
                        }
                    )

                }}>
                    <Row verti={"flex-end"}>
                        <InputSelect
                            setValue={setDriverTemplateID}
                            default={driverTemplateID}
                            padding={"20px"} label={translate("drivers_page.title")}
                            values={props.driverTemplates?.map(e => {
                                return { value: e._id, name: e.name }
                            })} />
                        <InputText
                            required
                            maxLength={16}
                            padding={"20px"}
                            key={uuid()}
                            backColor={mainColor}
                            icon={<Network style={{ fill: `${parentDetailsColor}`, height: "20px", width: "20px" }} />}
                            setValue={setIp}
                            defautlValue={ip}
                            name="ip" id="ip"
                            value={ip}
                            placeholder={'172.0.0.1'}
                            label={`${translate("scan_page.ip")}*`} />
                    </Row>

                    <MoreDetails
                        labelColor={"white"}
                        margin={"10px 0px 10px 0px"}
                        backColor={parentDetailsColor}
                        dividerColor={"transparent"}
                        title={translate("common.settings")}>
                        <MoreDetails
                            margin={"5px"}
                            dividerColor={parentDetailsColor}
                            backColor={detailsColor} title={"Snmp"}>
                            <Row><InputText
                                key={uuid()}
                                backColor={detailsColor}
                                type={"number"}
                                value={port}
                                setValue={setPort}
                                icon={<Ethernet
                                    style={{ fill: `${parentDetailsColor}`, height: "20px", width: "20px" }} />}
                                name="port" id="port"
                                label={translate("scan_page.port")} />
                                <InputSelect label={translate("common.version")}
                                    default={version}
                                    setValue={setVersion}
                                    values={[{ name: 'SNMP v1', value: 'SNMP v1' },
                                    { name: 'SNMP v2', value: 'SNMP v2' }]} />
                            </Row>
                        </MoreDetails>
                        <MoreDetails
                            open={false}
                            margin={"5px"}
                            backColor={detailsColor}
                            dividerColor={parentDetailsColor}
                            title={translate("common.advanced")}>
                            <Row> <InputText
                                key={uuid()}
                                setValue={setRetries}
                                type="number"
                                value={retries}
                                backColor={detailsColor}
                                icon={<Retry style={{ fill: `${parentDetailsColor}`, height: "20px", width: "20px" }} />}
                                name="retries" id="retries"
                                label={translate("scan_page.retries")} />
                                <InputText
                                    key={uuid()}
                                    setValue={setTimeout}
                                    type="number"
                                    value={timeout}
                                    backColor={detailsColor}
                                    icon={<Chrono
                                        style={{ fill: `${parentDetailsColor}`, height: "20px", width: "20px" }} />}
                                    name="timeout" id="timeout"
                                    label={translate("scan_page.timeout")} /></Row>
                            <Row><InputText
                                key={uuid()}
                                setValues={setMaxOid}
                                type="number"
                                value={maxOid}
                                backColor={detailsColor}
                                icon={<Max style={{ fill: `${parentDetailsColor}`, height: "20px", width: "20px" }} />}
                                name="oid" id="oid"
                                label={translate("scan_page.max_oid")} />
                                <InputText
                                    key={uuid()}
                                    type="number"
                                    value={delay}
                                    setValue={setDelay}
                                    backColor={detailsColor}
                                    icon={<Delay
                                        style={{ fill: `${parentDetailsColor}`, height: "20px", width: "20px" }} />}
                                    name="delay" id="delay"
                                    label={translate("scan_page.delay_between_requests")} />
                            </Row>
                        </MoreDetails>
                    </MoreDetails>


                    <Row style={{ width: "inherit", justifyContent: "space-evenly" }}>


                        <ButtonStyle
                            thememode={thememode}
                            style={{
                                width: "150px",
                                padding: "2px",
                                margin: "20px"
                            }}
                            onClick={(e) => {
                                e.preventDefault()
                                resetField()
                            }}
                            secondary={parentDetailsColor}
                            reverseColor>

                            <Row style={{ width: "100%" }} hori={'space-evenly'}>
                                <Reset style={{ width: "20px" }} />
                                <div>{translate("common.reset_all")}</div>
                            </Row>

                        </ButtonStyle>

                        <ButtonStyle
                            thememode={thememode}
                            type="submit"
                            style={{ padding: "2px", width: "150px", margin: "20px" }}
                            secondary={"#55a760"}
                            primary={getTheme(thememode, "color1")}
                            reverseColor
                        >
                            <Row style={{ width: "100%" }} hori={'space-around'}>
                                <Play style={{ width: "13px" }} />
                                <div>{translate("scan_page.launch_scan")}</div>
                            </Row>
                        </ButtonStyle>

                    </Row></form>
            </Container>
        </Dialog>
    </>
}