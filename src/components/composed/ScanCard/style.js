import styled from "styled-components";

export const Container = styled.div`
  padding: 15px;
  background-color: ${props => props.backColor ?? "#F1F0EA"};
  width: 50vw;
 
`