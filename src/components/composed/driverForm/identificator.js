import * as React from 'react';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';


export default function Identificator() {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Identificator
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="identificator"
            label="Identificator name"
            fullWidth
            autoComplete="Mac address"
            variant="standard"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="Identificator OID"
            label="Identificator OID"
            fullWidth
            autoComplete=".1.0.0.0.0.0.0"
            variant="standard"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="expValue"
            label="Expected value"
            fullWidth
            autoComplete="21212"
            variant="standard"
          />
        </Grid>


      </Grid>
    </React.Fragment>
  );
}
