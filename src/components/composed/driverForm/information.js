import * as React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';


export default function Information() {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Information
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="Name"
            name="Name"
            label="Name"
            fullWidth
            autoComplete="x53rD3"
            variant="standard"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="Company name"
            name="Company Name"
            label="Company name"
            fullWidth
            autoComplete="google"
            variant="standard"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="Image url"
            name="Image url"
            label="Image url"
            fullWidth
            autoComplete="https://google.com"
            variant="standard"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="Version"
            name="Version"
            label="Version"
            fullWidth
            autoComplete="1.0.0"
            variant="standard"
          />
        </Grid>


      </Grid>
    </React.Fragment>
  );
}
