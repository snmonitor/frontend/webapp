import {AES, enc} from "crypto-js"
import {SECRET} from "../env";

export default class User {

    static accessToken = '';
    static email = '';
    static username = '';
    static role = '';

    static storeData() {
        //crypt
        const accessToken = AES.encrypt(this.accessToken, SECRET);
        const email = AES.encrypt(this.email, SECRET);
        const username = AES.encrypt(this.username, SECRET);
        const role = AES.encrypt(this.role, SECRET);

        localStorage.setItem('accessToken', accessToken)
        localStorage.setItem('email', email)
        localStorage.setItem('username', username)
        localStorage.setItem('role', role)

    }

    static async getData() {
        //decrypt
        const username = localStorage.getItem('username');
        const email = localStorage.getItem('email');
        const accessToken = localStorage.getItem('accessToken')
        const role = localStorage.getItem('role')

        if (accessToken && email && username) {
            this.accessToken = (await AES.decrypt(accessToken, SECRET)).toString(enc.Utf8);
            this.email = (await AES.decrypt(email, SECRET)).toString(enc.Utf8);
            this.username = (await AES.decrypt(username, SECRET)).toString(enc.Utf8);
            this.role = (await AES.decrypt(role, SECRET)).toString(enc.Utf8);
            return this.toJson()
        } else {
            this.accessToken = null
            this.email = null
            this.username = null
            this.role = null
        }
    }

    static toJson() {
        return {
            username: this.username,
            accessToken: this.accessToken,
            email: this.email,
            role: this.role
        }
    }
}