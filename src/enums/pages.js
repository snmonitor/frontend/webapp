export const pages = {
    Dashboard: 'dashboard',
    Analytique: 'analytique',
    Scan: 'scan',
    Configuration: 'configuration',
    Driver: 'driverTemplate',
    Login: 'Login',
    App: 'app',
    Profile: 'Profile'
}