let baseUrl = "http://ec2-13-38-10-235.eu-west-3.compute.amazonaws.com/gateway/"
let socketUrl = "http://ec2-13-38-10-235.eu-west-3.compute.amazonaws.com"
let secret = "22f88314-9919-4140-9f7f-d439f6f9c2b2"

if (process.env.REACT_APP_STAGE === "dev") {
     baseUrl = "http://localhost:8080/"
     socketUrl = "http://localhost:1113/"
     secret = "22f88314-9919-4140-9f7f-d439f6f9c2b2"
}
export const BASE_URL = baseUrl
export const SOCKET_URL = socketUrl
export const SECRET = secret