import { isNullEmptyOrWhiteSpace } from "../Helper";

export function errorException(translation,data={},v=[],message=""){

                
return [
  //Success200Response
  
    {
         code:0,status: 200, message:translation("httpResponse.success"),data
      },
      

//Authenticated200Response
 {
        code: 1, status: 200, message: `${translation("httpResponse.authentificated")} : ${v[0]}`, data
    },

//BadRequest400Exception

     {
        code: 2, status: 400, message: `${translation("httpResponse.bad_request")} ${!isNullEmptyOrWhiteSpace(message)?": "+ message:""}`
    
},
//Created201Exception
{
        code: 3, status: 201, message: `${v[0]} ${translation("httpResponse.created")}`,data
    },

//Deleted200Exception
{
        code: 4, status: 200, message: `${v[0]} ${translation("httpResponse.deleted")}`,data
    }
,

// EmailAlreadyUsed409Exception 
 {
        code: 5, status: 409,
        message: `${translation("httpResponse.email_already_used1")} <${v[0]}> ${translation("httpResponse.email_already_used2")}`
    }
,

//EmailNotValide400Exception 
{
        code: 6, status: 400, message: `${translation("httpResponse.email_not_valide1")} <${v[0]}> ${translation("httpResponse.email_not_valide2")}`
    }
,


// FieldMissing400Exception
{
        code: 7, status: 400, message: `${translation("httpResponse.field_missing1")} ${v[0]} ${translation("httpResponse.field_missing2")}`,

    }
,

//Found200Response
{
        code: 8, status: 200, message: `${translation("httpResponse.found")} ${v[0]} ${v[1]}`, data,

    }
,

//IncorrectCredential401Exception
 {
        code: 9,
        status: 401,
        message: '${translation("httpResponse.incorrect_credential")}'
    }
,

//InternalError500Exception
{ code:10,status: 500, message:`${translation("httpResponse.internal_error")}`},

//NotAllowed403Exception 
 {code: 11, status: 403, message: `${translation("httpResponse.not_allowed")}`},


//NotFound404Exception 
{
        code: 12, status: 404, message: `${v[0]} ${translation("httpResponse.not_found")}`
    },

//NotUpdated400Exception 
{
        code: 13, status: 400, message: `${v[0]} ${translation("httpResponse.not_updated")}`,
    },

//NotUUID404Exception 
{
        code: 14, status: 404, message: `${translation("httpResponse.not_uuid1")} ${v[0]} ${translation("httpResponse.not_uuid2")}`
    },

//PasswordTooShort400Exception 
{
        code: 15, status: 400, message: `${translation("httpResponse.password_too_short")}`
    },

//Unauthorized401Exception 
{
        code: 16, status: 401, message:`${translation("httpResponse.unauthorized")} : ${v[0]}`
    },


//Updated200Response 
{
             code:17,status: 200, message:`${v[0]} ${translation("httpResponse.updated")}`,data
},

//WrongDevice400Response 
{
         code:18,status: 400, message:`${translation("httpResponse.wrong_device")} : ${v[0]} ; port: ${v[1]}`
},

//InvalidTemplateOID400Response 
{
         code:19,status: 400, message:`${translation("httpResponse.invalid_template_oid")}`
},

//NoDataReturnedFromOID400Response 
{
         code:20,status: 400, message:`${translation("httpResponse.no_data_returned")} :${v[0]}`
},

//IPHostNotFound400Response 
{
         code:21,status: 400, message:`${translation("httpResponse.ip_host_not_found1")} : ${v[0]} ${translation("httpResponse.ip_host_not_found2")} : ${v[1]}`
},

//ConnectedWithDevice200Response 
{
         code:22,status: 200, message:`${translation("httpResponse.connect_with_device")}`,data},
//JWTExpired401Response
{
         code:23,status: 401, message:`${translation("httpResponse.jwt_expired")}`},
];
}
