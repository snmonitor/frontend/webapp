import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { store } from "./redux/store";
import { Provider } from "react-redux";
import App from './App'
import fr from "./assets/langs/fr.json";
import en from "./assets/langs/en.json";
import { I18nextProvider } from "react-i18next";
import i18next from "i18next";
import { getDataFromLocalStorage, getMe } from './redux/auth/auth_service';
import Instance from './Instance';
import { NotificationsProvider } from '@mantine/notifications';
import { MantineProvider } from '@mantine/core';


i18next.init({
    interpolation: { escapeValue: false },
    lng: 'en',
    resources: {
        en: {
            common: en
        },
        fr: {
            common: fr
        },
    },
});


Instance.launcheSocket(store.dispatch)
store.dispatch(getDataFromLocalStorage)
store.dispatch(getMe)

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <MantineProvider>
                <NotificationsProvider>

                    <I18nextProvider i18n={i18next}>
                        <App />
                    </I18nextProvider>
                </NotificationsProvider>

            </MantineProvider>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
