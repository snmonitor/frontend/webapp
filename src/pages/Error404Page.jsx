import { NotFound } from "../assets/icons";
import { Center, getTheme } from "../components";
import { Column, Page } from "../components/style";
import { useTranslation } from "react-i18next";
import styled from 'styled-components'
import { useSelector } from "react-redux";

const Titler = styled.h1`
color:${p => getTheme(p.thememode, "color4")};

@media screen and (max-width: 900px) {
     font-size: x-large !important;
}
@media screen and (max-width: 715px) {
     font-size: large !important;
}
@media screen and (max-width: 585px) {
     font-size: 15px !important;
}
`

const Pic = styled(NotFound)`
fill:${p => getTheme(p.thememode, "color4")};
@media screen and (max-width: 900px) {
     height: 300px !important;
}
@media screen and (max-width: 715px) {
     height: 200px !important;
}
@media screen and (max-width: 585px) {
     height: 150px !important;
}
`

const SubTitle = styled.p`
color:${p => getTheme(p.thememode, "color4")};

@media screen and (max-width: 900px) {
     font-size: medium !important;
}
@media screen and (max-width: 715px) {
     font-size: small !important;
}
@media screen and (max-width: 585px) {
     font-size: x-small !important;
}
`

export default function Error404Page() {
     const thememode = useSelector(state => state.theme)
     const [translate] = useTranslation('common')
     return <Page
          style={{ marginTop: "20px" }}
          thememode={thememode}>
          <Center>
               <Column>
                    <Pic thememode={thememode} />
                    <Titler thememode={thememode}>
                         {translate('404_page.title').toUpperCase()}
                    </Titler>
                    <SubTitle thememode={thememode}>
                         {translate('404_page.subtitle').toUpperCase()}</SubTitle>
               </Column></Center></Page>
}