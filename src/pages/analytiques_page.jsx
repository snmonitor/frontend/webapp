
import { Grid, Skeleton, } from "@mui/material";
import { Area, AreaChart, LineChart, Line, Bar, BarChart, CartesianGrid, Legend, Pie, PieChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts";
import { Center, InfoBar, InputText, getTheme } from "../components";
import { useEffect, useMemo, useState } from "react";
import { getKpiHistoryByDriverId } from "../redux/kpi/kpi_service";
import { useDispatch, useSelector } from "react-redux";
import { isIterable } from "../Helper";
import { useTranslation } from "react-i18next";
import { ButtonStyle, Column, Page, Row, Title } from "../components/style";
import { v4 } from "uuid";
import { Chrono, Group, UnGroup } from "../assets/icons";
import { getAllDriver } from "../redux/driver/driver_service";
import { Switch } from "@mantine/core";
import moment from "moment"


export default function AnalytiquePage() {

    const thememode = useSelector(state => state.theme)

    const selectedLabelHeight = "90px"
    const colors = [`${getTheme(thememode, "colorChart")}`, "#809A6F", "#EF9F9F", "#A760FF", "#8CC0DE"]
    const severityColor = [`${getTheme(thememode, "colorError")}`,
    `${getTheme(thememode, "colorWarning")}`,
    `${getTheme(thememode, "colorSuccess")}`]
    const dispatch = useDispatch()
    const [translate] = useTranslation('common');
    const { donne: kpiHistory } = (useSelector(state => state?.kpi?.selectedDriver) ?? {})
    const { donne: drivers } = useSelector(state => state.driver)
    const [meters, setMeters] = useState(kpiHistory)
    const [indicators, setIndicators] = useState()
    const [indicatorsSum, setIndicatorsSum] = useState(false)
    const [metersByOID, setMetersByOID] = useState(false)
    const [selectedDriver, setSelectedDriver] = useState((drivers && isIterable(drivers ?? "")) && drivers[0])
    const [hours, setHours] = useState(1)
    const [minutes, setMinutes] = useState(0)

    const [realTime, setRealTime] = useState(true)
    const [group, setGroup] = useState(true)
    const kpi = (useSelector(state => state.kpi?.real) ?? "")


    useEffect(() => {
        dispatch(getAllDriver())
    }, [])

    useEffect(() => {
        if (selectedDriver == undefined && drivers && isIterable(drivers) && drivers?.length > 0)
            setSelectedDriver(drivers[0])
    }, [drivers])

    function dateFilter() {
        return moment(`${moment().add(-hours, 'hours').add(-minutes, 'minutes').format("YYYY-MM-DDTHH:mm:ss.ms")}`).toISOString()
    }
    useEffect(() => {
        if (selectedDriver?._id && realTime == true) {
            dispatch(getKpiHistoryByDriverId(selectedDriver?._id,
                dateFilter(),
                moment().toISOString()))
        }
    }, [kpi])

    useEffect(() => {
        if (selectedDriver?._id) {
            dispatch(getKpiHistoryByDriverId(selectedDriver?._id,
                dateFilter(),
                moment().toISOString()))

        }
    }, [selectedDriver, hours, minutes])

    useEffect(() => {

        if (isIterable(kpiHistory)) {

            const m = []


            kpiHistory?.map((e) => {
                if (isIterable(e.meters))
                    return e?.meters.map((e) => {
                        m.push({ ...e, createdAt: new Date(e?.createdAt)?.toUTCString() })
                        return e
                    })
            });
            setMeters(m)

            setIndicators(kpiHistory?.map((e) => e.indicators[0])?.filter(e => e && e)?.map((e) => {
                return { ...e, createdAt: new Date(e?.createdAt)?.toUTCString() }
            }))
        }
    }, [kpiHistory])


    // get the % for indicators
    useEffect(() => {
        if (isIterable(indicators)) {

            {
                let i = {}
                for (let indi of indicators) {
                    if (i[indi?.value]) {
                        i[indi?.value] = i[indi?.value] + 1
                    } else i[indi?.value] = 1
                }

                setIndicatorsSum(Object?.keys(i).map((e) => { return { name: e, value: i[e] } }))
            }

        } else
            setIndicatorsSum([])
    }, [indicators])


    // get meters by OID
    useEffect(() => {
        if (isIterable(meters)) {
            {
                let m = {}
                for (let meter of meters) {
                    if (m[meter?.identifier]) {
                        m[meter?.identifier].push(meter)
                    } else m[meter?.identifier] = [meter]
                }
                setMetersByOID(m)
            }

        } else
            setMetersByOID([])
    }, [meters])

    const dateFormatter = date => {
        if (hours > 24)
            return moment(date).format('DD/MM/YY');
        else
            return moment(date).format('HH:mm:ss');
    };

    const renderSwitch = useMemo(() => <Row
        width={"120px"} hori="space-between" >
        <Title
            thememode={thememode}
            style={{
                width: "100px",
                color: `${getTheme(thememode, " color4")}`, fontSize: "medium"
            }}>
            Real time</Title>
        <Switch
            radius="xs"
            checked={realTime} color="red" onLabel="ON" offLabel="OFF" onChange={(event) => {
                setRealTime(event.currentTarget.checked)
            }} /></Row >, [realTime, thememode, translate])

    const renderGroupButton = useMemo(() => {
        return <ButtonStyle
            thememode={thememode}
            secondary={"transparent"}
            primary={getTheme(thememode, "color4")}
            reverseColor
            style={{ padding: "0px", margin: "0" }} onClick={() => {
                if (group == true)
                    setGroup(false)
                else setGroup(true)
            }}>
            {group == true
                ? <UnGroup style={{ height: "12px" }} />
                : <Group style={{ height: "12px" }} />}

        </ButtonStyle>
    }, [group, thememode])

    const renderInput = useMemo(() => <Row><InputText
        hideIcon
        defaultValue={hours}
        backColor={getTheme(thememode, "color2")}
        color={getTheme(thememode, "color4")}
        label={`${translate("analytic_page.hours")}*`}
        height="10px"
        style={{ width: "100px", padding: "0" }}
        icon={<Chrono style={{
            fill: `${getTheme(thememode, "color4")}`,
            height: "20px", width: "20px"
        }} />}
        type="number"
        required

        key={v4()}
        setValue={setHours}
        name="hours" id="hours"
        value={hours}
        placeholder={`1 ${translate("analytic_page.hours")}`}
    ></InputText >
        <InputText
            hideIcon
            defaultValue={hours}
            backColor={getTheme(thememode, "color2")}
            color={getTheme(thememode, "color4")}
            label={`${translate("analytic_page.minutes")}*`}
            height="10px"
            style={{ width: "100px", padding: "0" }}
            icon={<div style={{
                fill: `${getTheme(thememode, "color4")}`,
                color: `${getTheme(thememode, "color4")}`,
                height: "5px", width: "5px"
            }} >:</div>}
            type="number"
            required

            key={v4()}
            setValue={setMinutes}
            name="minutes" id="minutes"
            value={minutes}
            placeholder={`10 ${translate("analytic_page.minutes")}`}
        ></InputText >
        <ButtonStyle
            thememode={thememode}
        >{translate('analytic_page.filter')}
        </ButtonStyle></Row>, [translate, thememode])


    const renderMetersChart = useMemo(() => {
        return metersByOID && metersByOID != false ?
            group == false ? selectedDriver?.version?.meters.map((e, index) => {
                return <Grid item xs={12}
                    style={{ margin: "0 0 20px 0px", padding: 0, height: '250px', width: "100%" }}
                    key={index}><ResponsiveContainer

                        key={index}
                        width="100%" height="100%">
                        <LineChart height="80%">

                            <XAxis
                                interval="preserveEnd"
                                allowDuplicatedCategory={false}
                                dataKey={"createdAt"}
                                angle={0}
                                tickFormatter={dateFormatter}

                            />
                            <YAxis />

                            <Tooltip />
                            <Legend />
                            <CartesianGrid

                                stroke={getTheme(thememode, "colorDisabled")} strokeDasharray="3 3" vertical={false} />
                            <Line
                                type="monotone"
                                data={metersByOID[e.identifier]}
                                dataKey={"value"}
                                stackId="1"
                                fill={index < colors?.length && colors[index]}
                                stroke={index < colors?.length && colors[index]}
                                name={e.identifier}
                                key={index}
                            />

                        </LineChart></ResponsiveContainer></Grid>
            })
                :
                <Grid item xs={12}
                    style={{ margin: 0, padding: 0, height: '250px', width: "100%" }}><ResponsiveContainer
                        width="100%" height="100%">
                        <LineChart height="80%">
                            <XAxis
                                interval="preserveEnd"
                                allowDuplicatedCategory={false}
                                dataKey={"createdAt"}
                                angle={0}
                                tickFormatter={dateFormatter}

                            />
                            <YAxis />

                            <Tooltip />
                            <Legend />
                            <CartesianGrid stroke={getTheme(thememode, "colorDisabled")} strokeDasharray="3" vertical={false} />
                            {
                                selectedDriver?.version?.meters.map((e, index) => {
                                    return <Line
                                        type="monotone"
                                        data={metersByOID[e.identifier]}
                                        dataKey={"value"}
                                        stackId="1"
                                        fill={index < colors?.length && colors[index]}
                                        stroke={index < colors?.length && colors[index]}
                                        name={e.identifier}
                                        key={index}
                                    />
                                })}
                        </LineChart></ResponsiveContainer></Grid>
            : <Center><Column style={{ margin: "0 0 20px 30px", width: "100%" }}>
                <Skeleton variant="rectangular" style={{
                    width: "100%"
                }} height={118} />
                <Skeleton variant="text" width={"100%"} /></Column></Center>
    }
        , [selectedDriver, metersByOID, colors, thememode, translate])

    const renderIndicatorChart = useMemo(() => {

        return indicatorsSum && indicatorsSum != false ?
            <ResponsiveContainer width="100%" height="80%">
                <BarChart
                    data={indicatorsSum}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <CartesianGrid stroke={getTheme(thememode, "colorDisabled")} strokeDasharray="3 3" vertical={false} />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    {indicatorsSum.map((e, index) => {
                        return <Bar
                            name={e.name}
                            key={index}
                            dataKey="value"
                            fill={colors[index]} />
                    })}
                </BarChart>
            </ResponsiveContainer >
            : <Center><Column style={{ margin: "0 0 20px 30px", width: "100%" }}>

                <Skeleton variant="rectangular" style={{
                    width: "100%"
                }} height={118} />
                <Skeleton variant="text" width={"100%"} /></Column></Center>
    }, [indicatorsSum, translate, thememode])


    const renderAlarmsChart = useMemo(() => {

        return kpiHistory && kpiHistory != false ?
            group == false ?
                ["severity1", "severity2", "severity3"].map((severity, index) =>
                    <Grid item xs={12}
                        key={index}
                        style={{ margin: "0 0 20px 0px", padding: 0, height: "250px", width: "100%" }}>
                        <ResponsiveContainer
                            key={index}
                            width="100%" height="100%">
                            <AreaChart height="80%"
                                data={kpiHistory}
                            >
                                <XAxis
                                    key={index}
                                    interval="preserveEnd"
                                    allowDuplicatedCategory={false}
                                    dataKey={"createdAt"}
                                    angle={0}
                                    tickFormatter={dateFormatter}

                                />
                                <YAxis allowDuplicatedCategory={false}
                                />

                                <Tooltip />

                                <Legend />
                                <CartesianGrid stroke={getTheme(thememode, "colorDisabled")} strokeDasharray="3 3" vertical={false} />
                                <Area

                                    connectNulls
                                    type="monotone"
                                    dataKey={severity}
                                    stackId="1"
                                    fill={index < colors?.length && severityColor[index]}
                                    stroke={index < colors?.length && severityColor[index]}
                                    name={["error", "warning", "success"][index]}
                                    key={index}
                                />
                            </AreaChart></ResponsiveContainer></Grid>)

                :
                <Grid item xs={12}
                    style={{ margin: 0, padding: 0, height: "250px", width: "100%" }}><ResponsiveContainer
                        width="100%" height="100%">
                        <AreaChart height="80%"
                        >
                            <XAxis
                                interval="preserveEnd"
                                allowDuplicatedCategory={false}
                                dataKey={"createdAt"}
                                angle={0}
                                tickFormatter={dateFormatter}

                            />
                            <YAxis allowDuplicatedCategory={false}
                            />

                            <Tooltip />

                            <Legend />
                            <CartesianGrid stroke={getTheme(thememode, "colorDisabled")} strokeDasharray="3 3" vertical={false} />
                            {
                                ["severity1", "severity2", "severity3"].map((e, index) => {

                                    return <Area
                                        data={kpiHistory}
                                        connectNulls
                                        type="monotone"
                                        dataKey={e}
                                        stackId="1"
                                        fill={index < colors?.length && severityColor[index]}
                                        stroke={index < colors?.length && severityColor[index]}
                                        name={["error", "warning", "success"][index]}
                                        key={index}
                                    />
                                })}
                        </AreaChart></ResponsiveContainer></Grid>
            : <Center><Column style={{ margin: "0 0 20px 30px", width: "100%" }}>
                <Skeleton variant="rectangular" style={{
                    width: "100%"
                }} height={118} />
                <Skeleton variant="text" width={"100%"} /></Column></Center>
    }
        , [kpiHistory, colors, translate, thememode])

    const renderPieChart = useMemo(() => {
        return indicatorsSum != false ?
            <ResponsiveContainer width="100%" height="80%">
                <PieChart >
                    <Pie
                        label data={indicatorsSum}
                        dataKey={"value"}
                        cx="50%" cy="50%"
                        outerRadius={100}
                        fill={colors[0]} />
                    <Tooltip />
                    <Legend />
                </PieChart>
            </ResponsiveContainer> : <Center><Column style={{ width: "90%" }}>

                <Skeleton variant="circular" style={{
                    width: "50%"
                }} height={118} />
                <Skeleton variant="text" width={"100%"} /></Column></Center>
    }, [indicatorsSum, translate, thememode])

    const renderInfoBar = useMemo(() =>
        <InfoBar setSelected={setSelectedDriver} />, [drivers, thememode, translate])

    function render() {

        return (
            <Page
                thememode={thememode}>
                <Grid container
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    columnSpacing={5}
                    rowSpacing={0}
                    style={{ height: "100%", width: "100%", padding: "0px 19px 25px 0px", margin: 0 }}
                    columns={{ xs: 3, md: 6, lg: 12, }}>

                    <Grid item xs={2.5} >
                        <Row

                            hori="center" height={selectedLabelHeight}>
                            <ButtonStyle
                                thememode={thememode}
                                shadow
                                primary={getTheme(thememode, "color4")}
                                secondary={getTheme(thememode, "color2")}
                                style={{
                                    border: "solid 0px",
                                    whiteSpace: 'nowrap',
                                    padding: "7px", fontSize: "large"
                                }}>{translate("scan_page.name")} : {selectedDriver?.name}</ButtonStyle>
                        </Row></Grid>

                    <Grid item xs={2.5} > <Row hori="center" height={selectedLabelHeight}>
                        <ButtonStyle
                            shadow
                            thememode={thememode}
                            primary={getTheme(thememode, "color4")}
                            secondary={getTheme(thememode, "color2")}
                            style={{
                                border: "solid 0px", whiteSpace: 'nowrap', padding: "7px", fontSize: "large"
                            }}>{translate("scan_page.ip")} : {selectedDriver?.IP}</ButtonStyle>
                    </Row></Grid>

                    <Grid item xs={2.5} > <Row hori="center" height={selectedLabelHeight}>
                        <ButtonStyle
                            thememode={thememode}
                            shadow
                            primary={getTheme(thememode, "color4")}
                            secondary={getTheme(thememode, "color2")}
                            style={{ border: "solid 0px", whiteSpace: 'nowrap', padding: "7px", fontSize: "large" }}>{translate("scan_page.protocol")} : {selectedDriver?.version?.protocol}</ButtonStyle>
                    </Row></Grid>

                    <Grid item xs={2.5} >
                        <Row
                            hori="center" height={selectedLabelHeight}>
                            <ButtonStyle
                                thememode={thememode}
                                shadow
                                primary={getTheme(thememode, "color4")}
                                secondary={getTheme(thememode, "color2")}
                                style={{ border: "solid 0px", whiteSpace: 'nowrap', padding: "7px", fontSize: "large" }}>{translate("scan_page.port")} : {selectedDriver?.port}</ButtonStyle>
                        </Row>
                    </Grid>

                    <Grid item xs={9.5}
                        style={{ margin: "0 20px 0px 30px", padding: 0, width: "100%" }}>
                        <Row hori="space-evenly" height={selectedLabelHeight}>

                            {renderSwitch}
                            {renderInput}
                            {renderGroupButton}

                        </Row></Grid>

                    <Grid item xs={12}
                        style={{
                            boxShadow: `2px 2px ${getTheme(thememode, "colorShadow")}`,
                            background: `${getTheme(thememode, "color2")}`, margin: "0 0 10px 30px", padding: "0 20px 0 0", width: "100%"
                        }}>

                        <Row hori="center" >
                            <Title
                                thememode={thememode}
                                style={{ height: "20px", fontSize: "large" }}>
                                {translate("dashboard_page.meters")}</Title></Row>
                        {renderMetersChart}</Grid>

                    <Grid item xs={12}
                        style={{ boxShadow: `2px 2px ${getTheme(thememode, "colorShadow")}`, background: `${getTheme(thememode, "color2")}`, margin: "0 0 10px 30px", padding: "0 20px 0 0", width: "100%" }}>
                        <Row hori="center">
                            <Title
                                thememode={thememode}
                                style={{ height: "20px", fontSize: "large" }}>
                                {translate("analytic_page.alarms")}</Title></Row>

                        {renderAlarmsChart}</Grid>

                    <Grid item xs={7}
                        style={{
                            boxShadow: `2px 2px ${getTheme(thememode, "colorShadow")}`,
                            background: `${getTheme(thememode, "color2")}`,
                            margin: "0 0 10px 30px", padding: "0 20px 0 0",
                            height: "300px", width: "100%"
                        }}>
                        <Row hori="center"><Title
                            thememode={thememode}
                            style={{ height: "20px", fontSize: "large" }}>
                            {translate("dashboard_page.indicators")}</Title></Row>
                        {renderIndicatorChart}
                    </Grid>

                    <Grid item xs={4}
                        style={{
                            boxShadow: `2px 2px ${getTheme(thememode, "colorShadow")}`,
                            background: `${getTheme(thememode, "color2")}`,
                            padding: "0", margin: "0 0 10px 30px",
                            width: "100%", height: "300px"
                        }}>
                        <Row hori="center"><Title
                            thememode={thememode}
                            style={{ height: "20px", fontSize: "large" }}>
                            {translate("dashboard_page.actions")}</Title></Row>
                        {renderPieChart}
                    </Grid>

                </Grid >
                {renderInfoBar}
            </Page>)
    }
    return useMemo(() => render(), [translate, thememode, indicatorsSum, group, realTime, metersByOID, selectedDriver])


}