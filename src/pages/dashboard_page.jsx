import { Grid, Switch } from "@mui/material";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { ListPage, SearchInput, Tabss, getTheme, Tree } from "../components";
import { ButtonStyle, Page, Row, Title } from '../components/style'
import { useEffect, useMemo, useState } from "react";
import { Maxi, Mini } from "../assets/icons";
import { isIterable } from "../Helper"
import { getAllDriver } from "../redux/driver/driver_service";

export default function DashboardPage() {

    //get la data du store
    const { donne: drivers, pagination } = useSelector(state => state.driver)
    const kpi = (useSelector(state => state.kpi?.real) ?? "")
    const [translate] = useTranslation('common');
    const [tab, setTab] = useState(0)
    const [searchValue, setSearchValue] = useState('')
    const [expand, setExpand] = useState({ xs: 3.5, md: 12 })
    const [selectedDriver, setSelectedDriver] = useState(isIterable(drivers ?? "") && drivers[0])
    const [kpis, setKpis] = useState([])
    const thememode = useSelector(state => state.theme)
    const dispatch = useDispatch()


    useEffect(() => {
        dispatch(getAllDriver())
    }, [])

    useEffect(() => {
        if (isIterable(drivers))
            setSelectedDriver(drivers[0])
    }, [drivers])

    useEffect(() => {
        setKpis(kpiByDriver())
    }, [kpi, selectedDriver])



    const tabLabls = [translate("dashboard_page.indicators"),
    translate("dashboard_page.informations"),
    translate("dashboard_page.meters"),
    translate("dashboard_page.actions")]



    function kpiByDriver() {
        for (let e of kpi) {
            if (e?.driverId == selectedDriver?._id) {
                return e
            }
        }
        return kpi
    }

    function manageTabs(index) {
        switch (index) {
            //indicators
            case 0:
                return {
                    component: false,
                    keys: ["identifier", "value", "createdAt"],
                    headers: ["oid", "value", "last update"],
                }
            //informations
            case 1:
                return {
                    component: false,
                    keys: ["identifier", "value", "createdAt"],
                    headers: ["oid", "value", "last update"],
                }
            //meters
            case 2:
                return {
                    component: false,
                    keys: ["identifier", "value", "createdAt"],
                    headers: ["oid", "value", "last update"],
                }
            //actions
            case 3:
                return {
                    component: true,
                    keys: ["identifier", "type", "value",],
                    headers: ["oid", "type", "value", "launch"],

                }
        }
    }
    const renderTree = useMemo(() => {
        return <Tree
            searchValue={searchValue}
            setSelectedElement={setSelectedDriver}
            elements={drivers}
            valueKey={"name"}
            style={{
                padding: "1vh 0 0 1vh",
                margin: "0",
                background: `${getTheme(thememode, "color2")}`,
                height: "58vh", width: "100%",
                overflowY: "auto",
            }} />
    }, [searchValue, drivers, kpi, translate, thememode])


    const render = () => {
        return <Page style={{ padding: "5px 30px 30px 30px" }}
            thememode={thememode}>
            <Grid container
                justifyContent="space-between"
                alignItems="flex-start"
                columnSpacing={2}
                rowSpacing={5}
                columns={expand}>
                <Grid item xs={3.5}>
                    <Title
                        thememode={thememode}
                    >{translate("dashboard_page.title")}</Title>
                    <SearchInput
                        value={searchValue}
                        setValue={setSearchValue}></SearchInput>
                    <Row hori="space-between"><Title
                        thememode={thememode}
                        style={{ fontSize: "large" }}>
                        {translate("drivers_page.title")}</Title>
                        <ButtonStyle
                            thememode={thememode}
                            secondary={"transparent"}
                            primary={getTheme(thememode, "color4")}
                            reverseColor
                            style={{ padding: "0px", margin: "0" }} onClick={() => {
                                if (expand.md == 12)
                                    setExpand({ xs: 3.5, md: 3.5 })
                                else setExpand({ xs: 3.5, md: 12 })
                            }}>
                            {expand.md == 12
                                ? <Maxi style={{ height: "12px" }} />
                                : <Mini style={{ height: "12px" }} />}

                        </ButtonStyle></Row>
                    {renderTree}
                </Grid>
                <Grid item xs={8.5}>

                    <Row hori="space-evenly" style={{ marginBottom: "10px" }}>
                        <Row hori="space-evenly">
                            <Title thememode={thememode} style={{ marginRight: "2px", fontSize: "large" }}>{translate("scan_page.name")}:</Title>
                            <Title thememode={thememode} style={{ fontSize: "large", fontWeight: "normal" }}>{selectedDriver?.name}</Title>
                        </Row>

                        <Row hori="space-evenly" >
                            <Title thememode={thememode} style={{ marginRight: "2px", fontSize: "large" }}>{translate("scan_page.ip")}:</Title>
                            <Title thememode={thememode} style={{ fontSize: "large", fontWeight: "normal" }}>{selectedDriver?.IP}</Title>
                        </Row>

                        <Row hori="space-evenly" >
                            <Title thememode={thememode} style={{ marginRight: "2px", fontSize: "large" }}>{translate("scan_page.protocol")}:</Title>
                            <Title thememode={thememode} style={{ fontSize: "large", fontWeight: "normal" }}>{selectedDriver?.version?.protocol}</Title>
                        </Row>

                        <Row hori="space-evenly">
                            <Title thememode={thememode} style={{ marginRight: "2px", fontSize: "large" }}>{translate("scan_page.port")}:</Title>
                            <Title thememode={thememode} style={{ fontSize: "large", fontWeight: "normal" }}>{selectedDriver?.port}</Title>
                        </Row>
                    </Row>
                    <Tabss
                        enabled={true}
                        labels={tabLabls}
                        value={tab}
                        setValue={setTab} />

                    <Title thememode={thememode} style={{ fontSize: "large" }}>{tabLabls[tab]}</Title>

                    <ListPage
                        key={0}
                        actions={tab == 3}
                        textHori={"flex-start"}
                        paginationSize={"small"}
                        readOnly={true}
                        component={
                            <div style={{ margin: "5px 0 5px 0", width: `25%` }}>
                                <Switch defaultChecked></Switch></div>
                        }
                        haveComponent={manageTabs(tab).component}
                        elementPerPage={0}
                        fontRatio={"4"}
                        style={{ width: "100%", padding: "0", }}
                        searchPlaceholder={translate("dashboard_page.placeholder")}
                        pagination={pagination}
                        elements={kpis[["indicators", "infos", "meters", "actions"][tab]]}
                        keys={manageTabs(tab).keys}
                        headers={manageTabs(tab).headers} />
                </Grid>
            </Grid></Page>
    }

    //use memo to fix the unwanted rerender when getting informations from socket
    const dashboard = useMemo(() => render(), [translate, thememode, searchValue, kpis, translate, tab, drivers, selectedDriver, expand])

    return dashboard
}




