
import { useSelector } from "react-redux";
import { Driver } from "../components/composed/driverForm";
import { Page } from "../components/style";

export default function DriverPage() {

    const thememode = useSelector(state => state.theme)


    return (<Page
        thememode={thememode}><Driver /></Page>)


}