import { useDispatch, } from "react-redux";
import { Login } from "../components";
import { login } from "../redux/auth/auth_service";
import { loadingAction } from "../redux/error/error_action";

export default function Login_page(props) {

    const dispatch = useDispatch()
    const submitLogin = (email, mdp, rememberMe = false) => {
        dispatch(loadingAction());
        dispatch(login(email, mdp, rememberMe))
    }
    return <Login auth={props.auth}


        submitLogin={submitLogin} />

}