import { useTranslation } from "react-i18next";
import { ButtonStyle, Column, Page } from "../components/style";
import { InfoBar, ListPage, SearchInput } from "../components";
import ScanCard from "../components/composed/ScanCard";
import { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getAllDriverTemplate } from "../redux/driverTemplate/driverTemplate_service";
import { createDriver, deleteDriver, getAllDriver } from "../redux/driver/driver_service";

export default function Scan() {

  const thememode = useSelector(state => state.theme)
  const [translate] = useTranslation('common');

  const elementPerPage = 8;
  const dispatch = useDispatch()

  //get tous les initial element
  useEffect(() => {
    dispatch(getAllDriverTemplate())
  }, [])

  //get la data du store
  const driverTemplates = useSelector(state => state.driverTemplate?.driverT)
  const { donne: drivers, pagination } = useSelector(state => state.driver)
  const [searchValue, setSearchValue] = useState('')


  async function deleteDriverHandler(id) {
    dispatch(deleteDriver(id, elementPerPage))
  }
  async function submitScanHandler(driver) {
    dispatch(createDriver(driver, elementPerPage))
  }

  const render = () => {
    return <Page
      thememode={thememode}
      style={{ display: "flex", padding: "5px 30px 5px 30px" }}>
      <Column hori="space-between" width="100%">
        <SearchInput
          value={searchValue}
          style={{ height: "45px", width: "80%", marginBottom: "30px" }}
          setValue={setSearchValue}></SearchInput>
        <ListPage
          constructor
          searchValue={searchValue}
          withPage
          elementPerPage={elementPerPage}
          pagination={pagination}
          getAllElements={getAllDriver}
          submit={submitScanHandler}
          delete={deleteDriverHandler}
          elements={drivers}
          fontRatio={"7"}
          style={{ width: "100%", padding: "0", }}
          keys={["name", "IP", "port", ["version", "protocol"]]}
          headers={[`${translate("scan_page.name").toUpperCase()}`,
          `${translate("scan_page.ip").toUpperCase()}`,
            'PORT',
          `${translate("scan_page.protocol").toUpperCase()}`,
          `${translate("scan_page.constructor").toUpperCase()}`]}
          title={translate('scan_page.title')}
          action={<ScanCard style={{ marginBottom: "30px", marginRight: "20vw" }}
            comp={<ButtonStyle
              thememode={thememode}
            >{translate('scan_page.button_scan')}
            </ButtonStyle>}
            submitFn={submitScanHandler}
            driverTemplates={driverTemplates} />}
        >
        </ListPage>
      </Column>
      <InfoBar />
    </Page >
  }

  //use memo to fix the unwanted rerender when getting informations from socket
  return useMemo(() => render(), [translate, thememode, drivers, searchValue, driverTemplates, pagination])

}