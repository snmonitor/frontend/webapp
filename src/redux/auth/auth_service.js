import { errorAction, loadingAction, } from "../error/error_action";
import { throwBackError } from "../../errors/errorManager";
import Instance from "../../Instance";
import { emailOrPasswordInc, getDataFromLocalStorageAction, loggedInAction, logoutAction } from "./auth_action";
import User from "../../entities/user";


export const login = (email, password, rememberMe) => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.post('users/authenticate',
                {
                    email: email,
                    password: password,
                    remember: rememberMe
                });
            dispatch(loadingAction(false))
            if (res.data.status > 300) {
                if (res.data.code === 9)
                    dispatch(emailOrPasswordInc())
                else
                    throwBackError(res.data, dispatch)
            }
            //success
            else
                dispatch(loggedInAction(res.data.donne, rememberMe))
        } catch (e) {
            dispatch(errorAction());
        }
    }
}

export const getDataFromLocalStorage = async (dispatch) => {
    try {
        dispatch(loadingAction())
        const user = await User.getData()
        dispatch(getDataFromLocalStorageAction(user))
        dispatch(loadingAction(false))

    } catch (e) {
        dispatch(errorAction({ code: 23 }));
    }
}


export const getMe = async (dispatch) => {
    try {
        dispatch(loadingAction())
        const axios = Instance.getAxios()
        const res = await axios.get('users/me');
        dispatch(loadingAction(false))

        if (res.status > 300 || res.data.status > 300) {
            return dispatch(logoutAction())
        }

    } catch (e) {
        dispatch(errorAction()
        );
    }
}





