export const GET_DRIVER = 'GET_DRIVER'
export const CREATE_DRIVER = "CREATE_DRIVER"
export const DELETE_DRIVER = "DELETE_DRIVER"

export function getAllDriverAction(drivers) {
    return {
        type: GET_DRIVER,
        payload: drivers,
    }
}

export function deleteDriverAction(driverId,limit=0) {
    return {
        type: DELETE_DRIVER,
        payload: driverId,
        limit:limit
    }
}

export function createDriverAction(driver,limit=0) {
    return {
        type: CREATE_DRIVER,
        payload: driver,
        limit:limit
    }
}