import {CREATE_DRIVER, DELETE_DRIVER, GET_DRIVER} from "./driver_action";

const initialState = {}


export default function reducer(state = initialState, action) {

    switch (action.type) {
        case GET_DRIVER:
            return action.payload;
            
        case CREATE_DRIVER:
            return {pagination:{"total":state.pagination?.total+1},
            donne:state.donne.length >= action.limit
            ? state.donne
            : [...state.donne,action.payload]}

        case DELETE_DRIVER:
            return {pagination:{"total":state.pagination?.total-1},donne:state.donne.filter((e)=>
            e._id!==action.payload
           )}
       
        default:
            return state
    }
}

