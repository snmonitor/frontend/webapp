import { errorAction, loadingAction, successAction, } from "../error/error_action";
import { throwBackError } from "../../errors/errorManager";
import Instance from "../../Instance";
import { createDriverAction, getAllDriverAction, deleteDriverAction } from "./driver_action";



export const getAllDriver = (skip, limit) => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.get('drivers', { params: { skip, limit } });
            dispatch(loadingAction(false))

            if (res.data.status > 300) {
                throwBackError(res.data, dispatch)
            }
            //success
            else
                dispatch(getAllDriverAction({ pagination: res.data.pagination, donne: res.data.donne }))
        } catch (e) {
            dispatch(errorAction({
                message: e.message,
                status: 500
            }))
        }
    }
}

export const createDriver = (driver, limit) => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.post('drivers/verification', driver);
            dispatch(loadingAction(false))

            if (res.data.status > 300) {
                throwBackError(res.data, dispatch)
            }
            //success
            else {
                dispatch(successAction(res.data))
                dispatch(createDriverAction(res.data.donne, limit))
            }
        } catch (e) {
            dispatch(errorAction())
        }
    }
}

export const deleteDriver = (id, limit = 0) => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.delete(`drivers/${id}`);
            dispatch(loadingAction(false))
            if (res.data.status > 300) {
                throwBackError(res.data, dispatch)
            }
            //success
            else {
                dispatch(successAction(res.data))
                dispatch(deleteDriverAction(res.data.donne._id, limit))
            }
        } catch (e) {
            dispatch(errorAction())
        }
    }
}

