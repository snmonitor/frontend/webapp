export const GET_DRIVER_TEMPLATE = 'GET_DRIVER_TEMPLATE'

export function getAllDriverTemplateAction(driverT) {
    return {
        type: GET_DRIVER_TEMPLATE,
        payload: {
            driverT: driverT,
        },
    }
}