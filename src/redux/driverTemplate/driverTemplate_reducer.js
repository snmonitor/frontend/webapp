//getting data from localstorage

import {GET_DRIVER_TEMPLATE} from "./driverTemplate_action";

const initialState = {}


export default function reducer(state = initialState, action) {

    switch (action.type) {
        case GET_DRIVER_TEMPLATE:
            return action.payload;
        default:
            return state
    }
}

