import { errorAction, loadingAction, } from "../error/error_action";
import { throwBackError } from "../../errors/errorManager";
import Instance from "../../Instance";
import { getAllDriverTemplateAction } from "./driverTemplate_action";


export const getAllDriverTemplate = () => {
    return async (dispatch) => {
        try {

            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.get('driverTemplates');
            dispatch(loadingAction(false))
            if (res.data.status > 300) {
                throwBackError(res.data, dispatch)
            }
            //success
            else
                dispatch(getAllDriverTemplateAction(res.data.donne))
        } catch (e) {
            dispatch(errorAction())
        }
    }
}




