export const ERROR = 'ERROR'
export const LOADING = 'LOADING'
export const SUCCESS = 'SUCESS'
export const WARN = "WARN"
export const INFO = "INFO"

export function errorAction(info =
    { code: 10 }) {
    return {
        type: ERROR,
        payload: info,
    }
}

export function infoAction(info =
    { code: 0 }) {
    return {
        type: INFO,
        payload: info,
    }
}

export function warningAction(info =
    { code: 2 }) {
    return {
        type: WARN,
        payload: info,
    }
}
export function successAction(info =
    { code: 0 }) {
    return {
        type: SUCCESS,
        payload: info,
    }
}

export function loadingAction(loading = true) {
    return {
        type: LOADING,
        payload: { loading: loading },
    }
}