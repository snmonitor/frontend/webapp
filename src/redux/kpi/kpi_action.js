export const KPI_RECEIVED = "KPI_RECEIVED"
export const GET_ALL_KPI_HISTORY = "GET_ALL_KPI_HISTORY"
export const GET_KPI_HISTORY_BY_DRIVERID = "GET_KPI_HISTORY_BY_DRIVERID"

export function kpiReceivedAction(kpis) {
    return {
        type: KPI_RECEIVED,
        payload: kpis
    }
}

export function getAllKpiHistoryAction(kpis) {
    return {
        type: GET_ALL_KPI_HISTORY,
        payload: kpis
    }
}

export function getKpiHistoryByDriverIdAction(kpis) {
    return {
        type: GET_KPI_HISTORY_BY_DRIVERID,
        payload: kpis
    }
}

