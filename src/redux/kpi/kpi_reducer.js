import {
    GET_ALL_KPI_HISTORY,
    GET_KPI_HISTORY_BY_DRIVERID,
    KPI_RECEIVED
} from './kpi_action';



const initialState = []


export default function reducer(state = initialState, action) {

    switch (action.type) {
        case GET_KPI_HISTORY_BY_DRIVERID:
            return {
                real: state?.real,
                history: state?.history,
                selectedDriver: action.payload
            };

        case GET_ALL_KPI_HISTORY:
            return {
                real: state?.real,
                history: action.payload,
                selectedDriver: state?.selectedDriver
            };

        case KPI_RECEIVED:
            return {
                real: action.payload,
                history: state?.history,
                selectedDriver: state?.selectedDriver,
            }


        default:
            return state
    }
}

