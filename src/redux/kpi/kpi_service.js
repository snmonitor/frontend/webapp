import { throwBackError } from "../../errors/errorManager";
import Instance from "../../Instance";
import { errorAction, } from "../error/error_action";
import { getAllKpiHistoryAction, getKpiHistoryByDriverIdAction } from "./kpi_action";

export const getAllKpiHistory = (skip, limit) => {
     return async (dispatch) => {
          try {

               const axios = Instance.getAxios()
               const res = await axios.get('kpihistorys', { params: { skip, limit } });

               if (res.data.status > 300) {
                    throwBackError(res.data, dispatch)
               }
               //success
               else
                    dispatch(getAllKpiHistoryAction({ pagination: res.data.pagination, donne: res.data.donne }))
          } catch (e) {
               dispatch(errorAction({
                    message: e.message,
                    status: 500
               }))
          }
     }
}


export const getKpiHistoryByDriverId = (driverId, from, to) => {
     return async (dispatch) => {
          try {

               const axios = Instance.getAxios()
               const res = await axios.post(`kpiHistorys/date`, {
                    "from": `${from}`,
                    "to": `${to}`,
                    "driverId": `${driverId}`
               }
               );

               if (res.data.status > 300) {
                    throwBackError(res.data, dispatch)
               }
               //success
               else
                    dispatch(getKpiHistoryByDriverIdAction({ donne: res.data.donne }))
          } catch (e) {
               dispatch(errorAction({
                    message: e.message,
                    status: 500
               }))
          }
     }
}