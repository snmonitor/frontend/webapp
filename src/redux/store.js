
import { configureStore } from '@reduxjs/toolkit'
import { user } from "./user"
import { error } from './error'
import { auth } from './auth'
import { driverTemplate } from './driverTemplate'
import { driver } from './driver'
import { kpi } from './kpi'
import { theme } from './theme'
export const store = configureStore({

     reducer: {
          user,
          theme,
          error,
          auth,
          driverTemplate,
          driver,
          kpi
     }
})


