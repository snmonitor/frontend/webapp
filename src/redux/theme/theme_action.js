export const DARK = "DARK"
export const LIGHT = "LIGHT"

export function darkAction() {
    return {
        type: DARK,
    }
}

export function lightAction() {
    return {
        type: LIGHT,
    }
}