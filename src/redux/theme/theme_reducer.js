import {
    DARK, LIGHT
} from './theme_action';


//getting data from localstorage

const initialState = DARK


export default function reducer(state = initialState, action) {

    switch (action.type) {
        case DARK:
            return DARK
        case LIGHT:
            return LIGHT
        default:
            return state
    }
}

