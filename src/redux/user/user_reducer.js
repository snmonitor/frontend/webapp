import { GET_USERS, USER_COUNT } from './user_action'

const initialState = {}


export default function reducer(state = initialState, action) {

    switch (action.type) {
        case GET_USERS:
            return { users: action.payload, count: state.count }
        case USER_COUNT:
            return { users: state.users, count: action.payload }
        default:
            return state
    }
}

